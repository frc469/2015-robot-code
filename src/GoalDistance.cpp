/*
 * GoalDistance.cpp
 *
 *  Created on: Feb 16, 2015
 *      Author: roryf_000
 */

#include "GoalDistance.h"
#include "Robot.h"


GoalDistance::GoalDistance(CANTalon *W) {
	// TODO Auto-generated constructor stub
	Current = W;
}

double GoalDistance::PIDGet()
{

	double Difference = ((Current->GetPosition() - robot->wrist_pot)*WRIST_POT_CONVERSION);
	return Difference;
}


GoalDistance::~GoalDistance() {
	// TODO Auto-generated destructor stub
}

