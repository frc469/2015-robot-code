/*
 * GoalDistance.h
 *
 *  Created on: Feb 16, 2015
 *      Author: roryf_000
 */

#ifndef GOALDISTANCE_H_
#define GOALDISTANCE_H_
#include "WPIlib.h"
#include <PIDSource.h>


class GoalDistance:public PIDSource

{
public:
	GoalDistance(CANTalon *W);
	virtual double PIDGet();
	virtual ~GoalDistance();
	CANTalon *Current;
};



#endif /* GOALDISTANCE_H_ */
