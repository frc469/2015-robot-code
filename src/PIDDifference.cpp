/*
 * PIDDifference.cpp
 *
 *  Created on: Feb 15, 2015
 *      Author: roryf_000
 */

#include "PIDDifference.h"
#include "wpilib.h"
#include "robot.h"

PIDDifference1::PIDDifference1(CANTalon *L, CANTalon *R, int C) {
	// TODO Auto-generated constructor stub
	Left = L;
	Right = R;
	if(C == 1)
	{
		Conversion = ENCODER_CONVERSION;
	}
	else if(C == 2)
	{
		Conversion = ARM_POT_CONVERSION;
	}
}

PIDDifference1::~PIDDifference1() {
	// TODO Auto-generated destructor stub
}

double PIDDifference1::PIDGet()
{

		double LeftPos = abs(Conversion*(double)Left->GetPosition());
		double RightPos = abs(Conversion*(double)Right->GetPosition());
		double Difference = (LeftPos-RightPos);
		return Difference;

	//return Difference;

}



