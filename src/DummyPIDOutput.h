/*
 * DummyPIDOutput.h
 *
 *  Created on: Feb 15, 2015
 *      Author: roryf_000
 */

#ifndef DUMMYPIDOUTPUT_H_
#define DUMMYPIDOUTPUT_H_
#include <PIDOutput.h>
#include "WPILib.h"

class DummyPIDOutput:public PIDOutput {
public:
	DummyPIDOutput();
	virtual ~DummyPIDOutput();
	virtual void PIDWrite(float output);
	float PIDResult;
	float GetPIDResult();
};

#endif /* DUMMYPIDOUTPUT_H_ */
