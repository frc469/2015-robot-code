#include "GrabToteToAuton.h"
#include "Robot.h"
#include "Subsystems/Stacker.h"
#include "Commands/DriveForward.h"
#include "Commands/DriveTurn.h"
#include "Commands/FloorOpenClose.h"
#include "Commands/FloorMotor.h"
#include "Commands/StackOpenClose.h"
#include "Commands/StackerPosition.h"

GrabToteToAuton::GrabToteToAuton()
{
	// A command group will require all of the subsystems that each member
	// would require.
	// e.g. if Command1 requires chassis, and Command2 requires arm,
	// a CommandGroup containing them would require both the chassis and the
	// arm.
	Requires(Robot::drivetrain);
	Requires(Robot::floorload);
	Requires(Robot::stacker);
	// Add Commands here:
	// e.g. AddSequential(new Command1());
	//      AddSequential(new Command2());
	// these will run in order.

	// To run multiple commands at the same time,
	// use AddParallel()
	// e.g. AddParallel(new Command1());
	//      AddSequential(new Command2());
	// Command1 and Command2 will run in parallel.
	AddSequential(new FloorOpenClose(2));
	AddSequential(new FloorMotor(FloorMotor::floorIn, false));
	AddSequential(new FloorOpenClose(1));
	AddSequential(new StackOpenClose(1));
	AddSequential(new StackerPosition(robot->stacker_load_1));
	AddSequential(new StackOpenClose(2));
	AddParallel(new StackerPosition(robot->stacker_clear_1));
	AddParallel(new DriveTurn(90));
	AddSequential(new DriveForward(7));
}
