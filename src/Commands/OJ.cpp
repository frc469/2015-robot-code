#include "OJ.h"
#include "FireOJ.h"
#include "DriveForwardGyro.h"
#include "robot.h"
#include "Drivetime.h"

OJ1::OJ1(int t)
{
	if(t == 1)
	{
		AddSequential(new FireOJ(1));
		AddSequential(new FireOJ(1));
		AddSequential(new WaitCommand(1));
//		AddSequential(new DriveForwardGyro(30, 1, 0));
		AddSequential(new FireOJ(2));
		AddSequential(new FireOJ(2));
		AddSequential(new WaitCommand(.1));
	}
	else if(t == 2)
	{
		AddSequential(new FireOJ(1));
		AddSequential(new FireOJ(1));
		AddSequential(new WaitCommand(1));
//		AddSequential(new DriveForwardGyro(30, 1, 0));
	}
	else if(t == 3)
	{
		AddSequential(new WaitCommand(.220));
		AddSequential(new DriveTime(1, .75));
	}
	// Add Commands here:
	// e.g. AddSequential(new Command1());
	//      AddSequential(new Command2());
	// these will run in order.

	// To run multiple commands at the same time,
	// use AddParallel()
	// e.g. AddParallel(new Command1());
	//      AddSequential(new Command2());
	// Command1 and Command2 will run in parallel.

	// A command group will require all of the subsystems that each member
	// would require.
	// e.g. if Command1 requires chassis, and Command2 requires arm,
	// a CommandGroup containing them would require both the chassis and the
	// arm.
}
