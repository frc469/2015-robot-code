#include "ArmManual.h"
#include "robot.h"
#include "Subsystems/arm.h"

ArmManual::ArmManual(int S)
{
	// Use Requires() here to declare subsystem dependencies
	Requires(Robot::arm);
	FirstManual = true;
	FirstPID = true;
	Type = S;
}

// Called just before this Command runs the first time
void ArmManual::Initialize()
{

}

// Called repeatedly when this Command is scheduled to run
void ArmManual::Execute()
{
	if(Type == 0)
	{
		float pid = robot->arm->AveragePIDResult->GetPIDResult();
		pidLeft = pid;
		pidRight = -pid;

		float WristPid = robot->arm->WristDistanceFromGoal->GetPIDResult();


		if(robot->JsOperator->GetRawAxis(1) >= .05 || robot->JsOperator->GetRawAxis(1) <= -.05)
		{

			FirstPID = true;
			if(FirstManual == true)
			{
				robot->LeftShoulder->SetVoltageRampRate(12);
				robot->RightShoulder->SetVoltageRampRate(12);
				FirstManual = false;
			}
			ManualRelease.Reset();
			ManualRelease.Start();
			robot->arm->ArmAverage->SetSetpoint(robot->arm->armPosition);
			robot->LeftShoulder->Set(-.5*robot->JsOperator->GetRawAxis(1));
			robot->RightShoulder->Set(.5*robot->JsOperator->GetRawAxis(1));
			motorSetting = .5*robot->JsOperator->GetRawAxis(1);

		}
		else
		{
			if(FirstPID == true)
			{
				robot->RightShoulder->Set(0);
				robot->LeftShoulder->Set(0);
				if(ManualRelease.HasPeriodPassed(motorSetting))
				{
					robot->LeftShoulder->SetVoltageRampRate(40);
					robot->RightShoulder->SetVoltageRampRate(40);
					robot->arm->ArmAverage->SetSetpoint(robot->arm->armPosition);
					FirstPID = false;
					FirstManual = true;
				}
			}
			else
			{
				robot->LeftShoulder->Set(robot->arm->pidLeft);
				robot->RightShoulder->Set(robot->arm->pidRight);
			}
		}


		//WristStuff


		float FeedManualArm;
		FeedManualArm = (1*robot->LeftShoulder->Get());
		if(FeedManualArm < 0.1)
			{
			FeedManualArm = 0.0;
			}
		robot->Wrist->Set(WristPid - FeedManualArm);
	}
	else if(Type == 1)
	{

	}
	else if(Type == 2)
	{

	}
	else if(Type == 3)
	{

	}


}

// Make this return true when this Command no longer needs to run execute()
bool ArmManual::IsFinished()
{
	return false;
}

// Called once after isFinished returns true
void ArmManual::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void ArmManual::Interrupted()
{

}
