#include "ThreeToteWithArm.h"
#include "FloorLoadAutomated.h"
#include "DriveForwardGyro.h"
#include "DriveForward.h"
#include "driveturn.h"
#include "floormotor.h"
#include "flooropenclose.h"
#include "armSet.h"
#include "GripToggle.h"
#include "CoutElapsedTime.h"
#include "AutonPath.h"
#include "ShiftGear.h"
#include "StackerPosition.h"
#include "AutonCurrentScan.h"
#include "robot.h"

ThreeToteWithArm::ThreeToteWithArm()
{
	AddParallel(new ArmSet(0, -90));
				AddParallel( new CoutElapsedTime( "before GripToggle(1)") );
	AddParallel(new GripToggle(1));
				AddParallel( new CoutElapsedTime( "before FloorMotor in") );
	AddParallel(new FloorMotor( FloorMotor::floorIn, true ));		// suck in first tote
				AddParallel( new CoutElapsedTime( "before FloorClose") );
	AddParallel(new FloorOpenClose(1));								// close rollers
				AddParallel( new CoutElapsedTime( "before WaitCommand(.5)") );
	AddSequential(new WaitCommand(.3));								// wait for tote to get pulled in
	//AddSequential(new FloorMotor( FloorMotor::floorIn, true ));		// turn rollers off
				AddParallel( new CoutElapsedTime( "before FloorLoadAutomated(6)") );
	AddSequential(new FloorLoadAutomated(6));						// elevate first tote
				AddParallel( new CoutElapsedTime( "before FloorMotor right") );
	AddSequential(new FloorMotor( FloorMotor::floorRight, true ));	// run wheels right
				AddParallel( new CoutElapsedTime( "before FloorOpenClose(1)") );
	AddSequential(new FloorOpenClose(1));							// open rollers
				AddParallel( new CoutElapsedTime( "before DriveForwardGyro(50,.75)") );
	AddSequential(new DriveForwardGyro(50.0, .68, 0));						// drive forward 30 inches to nudge can
				AddParallel( new CoutElapsedTime( "before WaitCommand(.5)") );
	AddSequential(new WaitCommand(.2));								// give time to shove can
				AddParallel( new CoutElapsedTime( "before ArmSet(-100,-90)") );
	AddParallel(new StackerPosition(robot->stacker_load_2+100));
	AddSequential(new ArmSet(-100, -90));
				AddParallel( new CoutElapsedTime( "before WaitCommand(1)") );
	AddSequential(new WaitCommand(1.0));
				AddParallel( new CoutElapsedTime( "before DriveForwardGyro(28,0.5)") );
	AddParallel (new AutonCurrentScan(1));
	AddSequential(new DriveForwardGyro(80.0, .5, 0));
				AddParallel( new CoutElapsedTime( "before GripToggle(2)") );
	AddParallel( new AutonCurrentScan(0) );
	AddSequential(new GripToggle(2));
	AddSequential(new WaitCommand(0.04));
				AddParallel( new CoutElapsedTime( "before ArmSet(0,-90)") );
	AddSequential(new ArmSet(0, -90));
				AddParallel( new CoutElapsedTime( "before WaitCommand(1)") );
	AddSequential(new WaitCommand(.5));
				AddParallel( new CoutElapsedTime( "before FloorMotor in") );
	AddSequential(new FloorMotor( FloorMotor::floorIn, true ));		// run rollers to pull second tote in
				AddParallel( new CoutElapsedTime( "before Floor open") );
	AddSequential(new FloorOpenClose(2));							// close rollers
				AddParallel( new CoutElapsedTime( "before DriveForwardGyro(30,.8)") );
	AddSequential(new DriveForwardGyro(33.0, .8, 0));						// drive forward 30 inches to nudge can
				AddParallel( new CoutElapsedTime( "before Floor close") );
	AddSequential(new FloorOpenClose(1));							// close floor rollers
				AddParallel( new CoutElapsedTime( "before WaitCommand(.5)") );
	AddSequential(new WaitCommand(.5));								// give time to shove can
				AddParallel( new CoutElapsedTime( "before FloorLoadAutomated(6)") );
	AddSequential(new FloorLoadAutomated(8));						// elevate totes 1+2
//				AddParallel( new CoutElapsedTime( "before Floor in") );
//	AddSequential(new FloorMotor( FloorMotor::floorIn, true ));		// run rollers to pull second tote in
	AddParallel(new FloorLoadAutomated(9));
				AddParallel( new CoutElapsedTime( "before Floor open") );
	AddParallel(new FloorOpenClose(2));
				AddParallel( new CoutElapsedTime( "before DriveForwardGyro(100,.8)") );
	AddSequential(new DriveForwardGyro(65.0, .8, 0));
				AddParallel( new CoutElapsedTime( "before Floor close") );
	AddParallel(new ShiftGear());
	AddSequential(new FloorOpenClose(1));
				AddParallel( new CoutElapsedTime( "Before Auton Path "));
	AddSequential(new AutonPath());
				AddParallel( new CoutElapsedTime( "Before FLoorLoadStop "));
	AddSequential(new FloorMotor(FloorMotor::floorIn, true));
				AddParallel( new CoutElapsedTime( "Before Drop it "));
	AddParallel(new FloorOpenClose(2));
	AddSequential(new FloorLoadAutomated(7));
				AddParallel( new CoutElapsedTime( "Before Drive Back "));
	AddSequential(new DriveForwardGyro(-10, .5, 0));
				AddSequential( new CoutElapsedTime( "Done "));
	// Add Commands here:
	// e.g. AddSequential(new Command1());
	//      AddSequential(new Command2());
	// these will run in order.

	// To run multiple commands at the same time,
	// use AddParallel()
	// e.g. AddParallel(new Command1());
	//      AddSequential(new Command2());
	// Command1 and Command2 will run in parallel.

	// A command group will require all of the subsystems that each member
	// would require.
	// e.g. if Command1 requires chassis, and Command2 requires arm,
	// a CommandGroup containing them would require both the chassis and the
	// arm.
}
