#ifndef DriveForward_H
#define DriveForward_H

#include "WPILib.h"
#include "PIDAverage.h"
#include "PIDDifference.h"
#include "DummyPIDOutput.h"

class DriveForward: public Command
{
private:
	bool Finished;
	PIDController *DriveStability;
	PIDController *SetPointDrive;
	PIDAverage1 *DistanceFromSetPoint;
	PIDDifference1 *DistanceFromEachother;
	DummyPIDOutput *AverageOutput;
	DummyPIDOutput *DifferenceOutput;
	Timer *TimeOut;
	double MaxSpeed;




public:
	double DriveDistance;
	DriveForward(double D);
	DriveForward(double D, double M);
	void init( double D, double M );
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
