#include "ThreeToteAuton.h"
#include "FloorLoadAutomated.h"
#include "DriveForwardGyro.h"
#include "StackOpenClose.h"
#include "DriveForward.h"
#include "driveturn.h"
#include "floormotor.h"
#include "flooropenclose.h"
#include "armSet.h"
#include "GripToggle.h"
#include "CoutElapsedTime.h"
#include "AutonPath.h"
#include "ShiftGear.h"
#include "StackerPosition.h"
#include "AutonCurrentScan.h"
#include "robot.h"

// 81.5" from back to back

ThreeToteAuton::ThreeToteAuton()
{


	AddParallel(new ArmSet(0, -90));     //leave the arm at top
	AddParallel(new GripToggle(1));		//open the wrist
	AddParallel(new FloorMotor( FloorMotor::floorIn, true ));		// suck in first tote
	AddParallel(new FloorOpenClose(1));								// close rollers
	//								// wait for tote to get pulled in
	//AddSequential(new FloorMotor( FloorMotor::floorIn, true ));		// turn rollers off
	//AddParallel(new FloorLoadAutomated(3));	// elevate first tote
	AddSequential(new WaitCommand(0.4));
	AddParallel(new FloorLoadAutomated(1));
	AddSequential(new DriveForwardGyro(0, 1, 70));
	AddParallel(new ArmSet(-115, -90));
	AddSequential(new WaitCommand(1.0));
	AddParallel(new DriveForward(-0.5, -0.75));
	AddSequential(new WaitCommand(1.0));		// wait for arm to come down
	AddSequential(new DriveForwardGyro(0, 0.75, -20));
	AddSequential(new DriveForwardGyro(2.0, 0.75, 0));
	AddSequential(new ShiftGear(1));
	AddSequential(new ArmSet(-95, -80));
	AddSequential(new DriveForwardGyro(0, 1, 0));


	AddSequential(new DriveForwardGyro(75.0, 0.8, 0));
	AddSequential(new GripToggle(2));								// pick up second can
	AddSequential(new WaitCommand(0.10));
	AddParallel(new ArmSet(0, -90));
	AddSequential(new DriveForwardGyro(23.0, 0.9, 0));
	AddSequential(new FloorOpenClose(1));							// close floor rollers
	AddSequential(new WaitCommand(.3));								// give time to shove can
	AddSequential(new FloorLoadAutomated(8));						// elevate totes 1+2
	AddParallel(new FloorLoadAutomated(9));
	AddParallel(new FloorOpenClose(2));
	AddSequential(new DriveForwardGyro(85.0, 0.9, 0));
	AddSequential(new FloorOpenClose(1));
	AddSequential(new DriveForwardGyro(0, 1.0, 80));
	//AddSequential(new ShiftGear(0));
	AddSequential(new DriveForwardGyro(110.0, 1.0, 80));
	AddSequential(new FloorMotor(FloorMotor::floorIn, true));
	AddSequential(new FloorOpenClose(2));
	AddSequential(new WaitCommand(0.10));
	AddSequential(new FloorLoadAutomated(7));
	AddSequential(new CoutElapsedTime( "Backing up "));
	AddSequential(new DriveForwardGyro(-30, 1.0, 90));
	AddSequential(new CoutElapsedTime( "Done "));

#ifdef CRAZY_BEHIND_THE_BACK_AUTON
	AddParallel(new ArmSet(110, 0));
	AddParallel(new StackerPosition(robot->stacker_clear_2));
	AddSequential(new WaitCommand(0.2));
	AddParallel(new ArmSet(469, 90));
	AddSequential(new WaitCommand(0.5));
	AddParallel(new GripToggle(2));
	AddSequential(new WaitCommand(0.2));
	AddParallel(new ArmSet(-15, 90));
	AddSequential(new WaitCommand(2.0));
	AddParallel(new FloorMotor( FloorMotor::floorIn, true ));		// suck in first tote
	AddSequential(new FloorOpenClose(1));								// close rollers
	AddSequential(new WaitCommand(0.1));
	AddSequential(new DriveForwardGyro(12, 0.4, 0));
	AddSequential(new FloorLoadAutomated(6));						// elevate first tote

	AddParallel(new DriveForwardGyro(0, 0.4, 180));				// turn right 180
	AddSequential(new WaitCommand(1.5));

	AddSequential(new DriveForwardGyro(50, 0.7, 0));
#endif
}
