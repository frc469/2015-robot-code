#include "StackerPosition.h"
#include "robot.h"
#include "../subsystems/Stacker.h"

StackerPosition::StackerPosition( int position )
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires( Robot::stacker );

	setPosition = position;
	Finished = false;
	skip = 0;
}

// Called just before this Command runs the first time
void StackerPosition::Initialize()
{
	double position;

	position = robot->Stack->GetPosition(); //GetAnalogInRaw();
	skip = 0;

	SetTimeout( Stacker::STACKER_TIMEOUT );
	// if the elevator is getting close, slow it down
	if( abs( position - setPosition ) < Stacker::STACKER_TOLERANCE )
	{
		// Already at the target position
		Finished = true;
		End();
	}
	else if( position < setPosition )
	{
		// Always go up at top speed
		// Slower speeds may not be able to move up when carrying totes!
		robot->Stack->Set( 1.0 );
		Finished = false;
	}
	else
	{
		// Going down, nearing target (within 100 counts)
		robot->Stack->Set( -1.0 );
		Finished = false;
	}
}

// Called repeatedly when this Command is scheduled to run
void StackerPosition::Execute()
{
	double position;
	double stackMoving;

	position = robot->Stack->GetPosition(); //AnalogInRaw();
	stackMoving = robot->Stack->Get();

	++skip;
	if( skip >= 5 )
	{
		std::cout << "StackerPosition:Execute() position=" << position << ", set=" << setPosition << "\n";
		skip = 0;
	}

	// if the elevator is getting close, slow it down
	if( abs( position - setPosition ) < Stacker::STACKER_TOLERANCE )
	{
		// Already at the target position
		Finished = true;
		End();
	}
	else if( stackMoving < 0)
	{
		// going down
		if( position < setPosition )
		{
			Finished = true;
			End();
		}
	}
	else if( stackMoving > 0 )
	{
		// going up
		if( position > setPosition )
		{
			Finished = true;
			End();
		}
	}
	else if( (IsTimedOut() == true) || (robot->emergencyStop == true) )
	{
		Finished = true;
		End();
	}
}

// Make this return true when this Command no longer needs to run execute()
bool StackerPosition::IsFinished()
{
	return( Finished );
}

// Called once after isFinished returns true
void StackerPosition::End()
{
	robot->Stack->Set(0);
	Finished = true;
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void StackerPosition::Interrupted()
{
	End();
}
