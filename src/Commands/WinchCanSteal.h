#ifndef WinchCanSteal_H
#define WinchCanSteal_H


#include "WPILib.h"

class WinchCanSteal: public Command
{
public:
	WinchCanSteal();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:
	Timer *T;
	bool Spooled;
	bool Finished;
};

#endif
