#include "Btn1.h"
#include "Robot.h"
#include "Commands/FloorOpenClose.h"
#include "Commands/ArmSet.h"

Btn1::Btn1()
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires(Robot::dummy);
}

// Called just before this Command runs the first time
void Btn1::Initialize()
{
	Finished = false;
}

// Called repeatedly when this Command is scheduled to run
void Btn1::Execute()
{
	if (robot->OperatorMode == 1)
	{
		robot->Btn1Mode1->Start();
	}
	else if (robot->OperatorMode == 2)
	{
		robot->Btn1Mode2->Start();
	}

	Finished = true;
}

// Make this return true when this Command no longer needs to run execute()
bool Btn1::IsFinished()
{
	return Finished;
}

// Called once after isFinished returns true
void Btn1::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void Btn1::Interrupted()
{

}
