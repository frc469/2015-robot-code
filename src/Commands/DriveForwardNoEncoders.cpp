#include "DriveForwardNoEncoders.h"
#include "robot.h"

DriveForwardNoEncoders::DriveForwardNoEncoders(double D, double M)
{
	TimeOut = new Timer;
	DriveDistance = D;
	DistanceFromSetPoint = new PIDAverage1(robot->RightDrive1, robot->RightDrive1, 0, 0, ENCODER_CONVERSION, ENCODER_CONVERSION);
	DistanceFromEachother = new PIDDifference1(robot->RightDrive1, robot->RightDrive1, 1);
	AverageOutput = new DummyPIDOutput();
	DifferenceOutput = new DummyPIDOutput();
	DriveStability = new PIDController(.025, 0.0, 0.0, DistanceFromEachother, DifferenceOutput, .02);
	DriveStability->SetSetpoint(0);
	DriveStability->Enable();
	SetPointDrive = new PIDController(.065, 0.0, 0.0, DistanceFromSetPoint, AverageOutput, .02);
	SetPointDrive->SetSetpoint(D);
	SetPointDrive->Enable();
	Finished = false;
	MaxSpeed = M;
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
}

// Called just before this Command runs the first time
void DriveForwardNoEncoders::Initialize()
{
	Finished = false;
	robot->LeftDrive1->SetPosition(0);
	robot->RightDrive1->SetPosition(0);
	TimeOut->Reset();
	TimeOut->Start();
	DriveStability->Enable();
	SetPointDrive->Enable();
}

// Called repeatedly when this Command is scheduled to run
void DriveForwardNoEncoders::Execute()
{
	float Result;
		if(AverageOutput->GetPIDResult() >= MaxSpeed)
		{
			Result = MaxSpeed;
		}
		else if(AverageOutput->GetPIDResult() <= MaxSpeed)
		{
			Result = -MaxSpeed;
		}
		else
		{
			Result = AverageOutput->GetPIDResult();
		}



	#if DRIVE_FORWARD_DEBUG
		std::cout << "SetPoint is" << DriveDistance << "Position is " << robot->LeftDrive1->GetPosition() << "Setpoint Output is " << AverageOutput->GetPIDResult() << "Difference output is" << DifferenceOutput->GetPIDResult();
		std::cout << "How far from eachother" << DistanceFromEachother->PIDGet() << "\n";
	#endif
		if((DriveDistance - DistanceFromSetPoint->PIDGet()) < 1 && (DriveDistance - DistanceFromSetPoint->PIDGet()) > -1)
		{
			robot->LeftDrive1->Set(0);
			robot->RightDrive1->Set(0);
			Finished = true;
		}
		else if((DistanceFromEachother->PIDGet()) > .25)
		{
			robot->LeftDrive1->Set(-(Result + DifferenceOutput->GetPIDResult()));
			robot->RightDrive1->Set(-Result);
		}
		else if((DistanceFromEachother->PIDGet()) < -.25)
		{
			robot->LeftDrive1->Set(-Result);
			robot->RightDrive1->Set((-(Result - DifferenceOutput->GetPIDResult())));
		}
		else
		{
			robot->LeftDrive1->Set(-Result);
			robot->RightDrive1->Set(-Result);
		}
}

// Make this return true when this Command no longer needs to run execute()
bool DriveForwardNoEncoders::IsFinished()
{
	if((TimeOut->HasPeriodPassed(5.0) == true) || (Finished == true))
	{
		TimeOut->Stop();
		TimeOut->Reset();
		End();
		return true;
	}
	else
	{
		return false;
	}

}

// Called once after isFinished returns true
void DriveForwardNoEncoders::End()
{
	robot->LeftDrive1->SetPosition(0);
	robot->RightDrive1->SetPosition(0);
	DriveStability->Disable();
	SetPointDrive->Disable();
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void DriveForwardNoEncoders::Interrupted()
{

}
