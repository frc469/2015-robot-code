#include "ShiftGear.h"
#include "robot.h"

ShiftGear::ShiftGear()
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires(Robot::drivetrain);
	finished = false;
	init( 0, true );
}

ShiftGear::ShiftGear( int state )
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires(Robot::drivetrain);
	init( state, false );
	finished = false;
}

void ShiftGear::init( int state, bool toggle )
{
	shift_state = state;
	shift_toggle = toggle;
}

// Called just before this Command runs the first time
void ShiftGear::Initialize()
{
	finished = false;
}

// Called repeatedly when this Command is scheduled to run
void ShiftGear::Execute()
{
	if( shift_toggle == true )
	{
		if( robot->gearHigh )
		{
			robot->Drive->Set( true );
			robot->gearHigh = false;
		}
		else
		{
			robot->Drive->Set( false );
			robot->gearHigh = true;
		}
	}
	else
	{
		robot->Drive->Set( shift_state );
		if( shift_state )
		{
			robot->gearHigh = false;
		}
		else
		{
			robot->gearHigh = true;
		}
	}
	finished = true;
	std::cout << "gearHigh=" << robot->gearHigh << "\n";
}

// Make this return true when this Command no longer needs to run execute()
bool ShiftGear::IsFinished()
{
	return finished;
}

// Called once after isFinished returns true
void ShiftGear::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void ShiftGear::Interrupted()
{
}
