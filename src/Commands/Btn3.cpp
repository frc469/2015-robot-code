#include "Btn3.h"
#include "Robot.h"
#include "Commands/ArmSet.h"

Btn3::Btn3()
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires(Robot::dummy);
}

// Called just before this Command runs the first time
void Btn3::Initialize()
{
	Finished = false;
}

// Called repeatedly when this Command is scheduled to run
void Btn3::Execute()
{
	if (robot->OperatorMode == 1)
	{
		robot->Btn3Mode1->Start();
		//do nothing
	}
	else if (robot->OperatorMode == 2)
	{
		robot->Btn3Mode2->Start();
	}

	Finished = true;
}

// Make this return true when this Command no longer needs to run execute()
bool Btn3::IsFinished()
{
	return Finished;
}

// Called once after isFinished returns true
void Btn3::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void Btn3::Interrupted()
{

}
