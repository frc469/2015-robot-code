#include "Interrupt.h"
#include "robot.h"


Interrupt::Interrupt()
{
	Requires(Robot::stacker);
	Requires(Robot::arm);
	Requires(Robot::floorload);
	Requires(Robot::drivetrain);
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
}

// Called just before this Command runs the first time
void Interrupt::Initialize()
{
	robot->emergencyStop = true;
}

// Called repeatedly when this Command is scheduled to run
void Interrupt::Execute()
{
	End();
}

// Make this return true when this Command no longer needs to run execute()
bool Interrupt::IsFinished()
{
	return true;
}

// Called once after isFinished returns true
void Interrupt::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void Interrupt::Interrupted()
{

}
