#include "WinchCanSteal.h"
#include "subsystems/CanSteal.h"
#include "robot.h"
#include <iostream>

WinchCanSteal::WinchCanSteal()
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires(Robot::cansteal);
	Finished = false;
	Spooled = false;
	T = new Timer;
}

// Called just before this Command runs the first time
void WinchCanSteal::Initialize()
{
	Finished = false;
	Spooled = false;
	T->Reset();
	T->Start();
}

// Called repeatedly when this Command is scheduled to run
void WinchCanSteal::Execute()
{

	std::cout << "Winch Current is " << robot->CanStealMotor->GetOutputCurrent() << "\n";
	if((robot->CanStealMotor->GetOutputCurrent() >= 100) && (T->HasPeriodPassed(.5) == true))
	{
		Spooled = true;
	}
	if(Spooled == false)
	{
		robot->CanStealMotor->Set(-1);
	}
	else if(Spooled == true)
	{
		robot->CanStealMotor->Set(0);
		Finished = true;
		/*robot->CanStealMotor->Set(0);
		T->Reset();
		if(T->HasPeriodPassed(.5))
		{
			Spooled = false;
		}*/

	}
	if(robot->JsDriver->GetPOV() != 270)
	{
		robot->CanStealMotor->Set(0);
		Finished = true;
	}
}

// Make this return true when this Command no longer needs to run execute()
bool WinchCanSteal::IsFinished()
{
	return Finished;
}

// Called once after isFinished returns true
void WinchCanSteal::End()
{
	robot->CanStealMotor->Set(0);
	Finished = true;
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void WinchCanSteal::Interrupted()
{

}
