#include "AutonCurrentScan.h"
#include "robot.h"

AutonCurrentScan::AutonCurrentScan(bool Y)
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	scan = Y;
}

// Called just before this Command runs the first time
void AutonCurrentScan::Initialize()
{
	robot->autonCurrentScan = scan;
}

// Called repeatedly when this Command is scheduled to run
void AutonCurrentScan::Execute()
{

}

// Make this return true when this Command no longer needs to run execute()
bool AutonCurrentScan::IsFinished()
{
	return true;
}

// Called once after isFinished returns true
void AutonCurrentScan::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void AutonCurrentScan::Interrupted()
{

}
