#include "ZeroArm.h"

#include "robot.h"

#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include "stdio.h"

ZeroArm::ZeroArm()
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
}

// Called just before this Command runs the first time
void ZeroArm::Initialize()
{

}

// Called repeatedly when this Command is scheduled to run
void ZeroArm::Execute()
{

}

// Make this return true when this Command no longer needs to run execute()
bool ZeroArm::IsFinished()
{
	return false;
}

// Called once after isFinished returns true
void ZeroArm::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void ZeroArm::Interrupted()
{

}
