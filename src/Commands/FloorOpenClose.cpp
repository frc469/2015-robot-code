#include "FloorOpenClose.h"
#include "robot.h"


FloorOpenClose::FloorOpenClose( int t )
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	//Requires( Robot::floorload );
	Type = t;
//	robot->FloorOpen = robot->FloorSolenoid->Get();
	finished = false;
}

// Called just before this Command runs the first time
void FloorOpenClose::Initialize()
{
	robot->FloorFinished = false;
	finished = false;
}

// Called repeatedly when this Command is scheduled to run
void FloorOpenClose::Execute()
{
	if(Type == 0)
	{

		if(robot->FloorOpen == false)
		{
			robot->FloorSolenoid->Set(false);
			robot->FloorOpen = true;
		}
		else
		{
			robot->FloorSolenoid->Set(true);
			robot->FloorOpen = false;
		}
	}
	else if(Type == 1)
	{
		robot->FloorSolenoid->Set(true);
		robot->FloorOpen = false;
	}
	else if(Type == 2)
	{
		robot->FloorSolenoid->Set(false);
		robot->FloorOpen = true;
	}
	robot->FloorFinished = true;
	finished = true;

}

// Make this return true when this Command no longer needs to run execute()
bool FloorOpenClose::IsFinished()
{
	return finished;
}

// Called once after isFinished returns true
void FloorOpenClose::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void FloorOpenClose::Interrupted()
{

}
