#include "GrabCanWithArmAuton.h"
#include "startezggnore.h"
#include "DriveForwardGyro.h"
#include "SetDown.h"

GrabCanWithArmAuton::GrabCanWithArmAuton()
{
	AddSequential(new StartEZGGNore(2));
	AddSequential(new DriveForwardGyro(0, 0, -90));
	AddSequential(new DriveForwardGyro(80, .76, 0));
	AddSequential(new SetDown());
	AddSequential(new DriveForwardGyro(0, 0, 90));
	AddSequential(new StartEZGGNore(2));


	// Add Commands here:
	// e.g. AddSequential(new Command1());
	//      AddSequential(new Command2());
	// these will run in order.

	// To run multiple commands at the same time,
	// use AddParallel()
	// e.g. AddParallel(new Command1());
	//      AddSequential(new Command2());
	// Command1 and Command2 will run in parallel.

	// A command group will require all of the subsystems that each member
	// would require.
	// e.g. if Command1 requires chassis, and Command2 requires arm,
	// a CommandGroup containing them would require both the chassis and the
	// arm.

}
