#include "GripToggle.h"
#include "robot.h"
#include "subsystems/arm.h"

GripToggle::GripToggle(int t)
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	//Requires(Robot::arm);
	Type = t;
	Finished = false;
}

// Called just before this Command runs the first time
void GripToggle::Initialize()
{
	Finished = false;
}

// Called repeatedly when this Command is scheduled to run
void GripToggle::Execute()
{
	if(Type == 0)
		{
			if(robot->arm->GripOpen == true)
			{
				robot->CanGrab->Set(DoubleSolenoid::kReverse);
				robot->arm->GripOpen = false;
			}
			else
			{
				robot->CanGrab->Set(DoubleSolenoid::kForward);
				robot->arm->GripOpen = true;
			}
		}
		else if(Type == 1)
		{
			robot->CanGrab->Set(DoubleSolenoid::kForward);
			robot->arm->GripOpen = true;
		}
		else if(Type == 2)
		{
			robot->CanGrab->Set(DoubleSolenoid::kReverse);
			robot->arm->GripOpen = false;
		}
		else if(Type == 3)
		{
			robot->CanGrab->Set(DoubleSolenoid::kOff);
			robot->arm->GripOpen = true;
		}
		Finished = true;
 }

// Make this return true when this Command no longer needs to run execute()
bool GripToggle::IsFinished()
{
	return Finished;
}

// Called once after isFinished returns true
void GripToggle::End()
{
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void GripToggle::Interrupted()
{

}
