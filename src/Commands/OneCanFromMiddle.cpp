#include "OneCanFromMiddle.h"
#include "armset.h"
#include "GripToggle.h"
#include "DriveForward.h"
#include "DriveTime.h"
#include "DriveForwardGyro.h"
#include "DriveTime.h"

OneCanFromMiddle::OneCanFromMiddle()
{
	AddSequential(new ArmSet(90, -90));
	AddSequential(new WaitCommand(1));
	AddSequential(new ArmSet(90, -90));
	AddSequential(new WaitCommand(1));
	AddSequential(new ArmSet(90, 90));
	AddSequential(new WaitCommand(1));
	AddSequential(new DriveTime(.5, .5));
	AddSequential(new WaitCommand(.2));
	AddSequential(new GripToggle(2));
	AddSequential(new WaitCommand(.2));
	AddSequential(new DriveTime(-.5, .3));
	AddSequential(new WaitCommand(.2));
	AddSequential(new GripToggle(.2));
	AddSequential(new WaitCommand(.2));
	AddSequential(new DriveTime(.5, .5));
	AddSequential(new WaitCommand(.2));
	AddSequential(new GripToggle(2));
	AddSequential(new ArmSet(0, 90));


	// Add Commands here:
	// e.g. AddSequential(new Command1());
	//      AddSequential(new Command2());
	// these will run in order.

	// To run multiple commands at the same time,
	// use AddParallel()
	// e.g. AddParallel(new Command1());
	//      AddSequential(new Command2());
	// Command1 and Command2 will run in parallel.

	// A command group will require all of the subsystems that each member
	// would require.
	// e.g. if Command1 requires chassis, and Command2 requires arm,
	// a CommandGroup containing them would require both the chassis and the
	// arm.
}
