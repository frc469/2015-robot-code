#include <Commands/AutonPath.h>
#include "Robot.h"

// This is a table of left/right drive speeds to play back a motion profile.
// It has to end in 469 or it'll run past the end of the table and crash.
// TODO: figure out either 254's TrajectoryPath, or get it from somewhere else!

float driveProfile[] = {
		0.100	,	0.000	,
		0.200	,	0.000	,
		0.300	,	0.000	,
		0.400	,	0.000	,
		0.500	,	0.000	,
		0.600	,	0.000	,
		0.700	,	0.000	,
		0.800	,	0.000	,
		0.900	,	0.000	,
		1.000	,	0.000	,
		1.000	,	0.000	,
		1.000	,	0.000	,
		1.000	,	0.000	,
		1.000	,	0.000	,
		1.000	,	0.000	,
		1.000	,	0.000	,
		1.000	,	0.000	,
		1.000	,	0.000	,
		1.000	,	0.000	,
		1.000	,	0.000	,
		1.000	,	0.000	,
		1.000	,	0.000	,
		1.000	,	0.000	,
		1.000	,	0.000	,
		1.000	,	0.200	,
		1.000	,	0.400	,
		1.000	,	0.600	,
//		1.000	,	0.800	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000   ,
		1.000	,	1.000   ,
		1.000	,	1.000   ,
		1.000	,	1.000   ,
		1.000	,	1.000   ,
		0.000   ,   0.000
};

AutonPath::AutonPath()
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires(Robot::drivetrain);
	driveTimer = new Timer();
	bDone = false;
	motorIndex = 0;
}

// Called just before this Command runs the first time
void AutonPath::Initialize()
{
	SetTimeout(3);
	bDone = false;
	motorIndex = 0;
	driveTimer->Reset();
	driveTimer->Start();
}

// Called repeatedly when this Command is scheduled to run
void AutonPath::Execute()
{
	if ( motorIndex < (int) (sizeof(driveProfile) / sizeof(driveProfile[0])) )
	{
		if (driveTimer->Get() > motorIndex * 0.025)
		{
			robot->LeftDrive1->Set(-driveProfile[motorIndex]);
			robot->RightDrive1->Set(-driveProfile[motorIndex+1]);
			motorIndex += 2;
		}
	}
	else
	{
		bDone = true;
	}
}

// Make this return true when this Command no longer needs to run execute()
bool AutonPath::IsFinished()
{
	if( (bDone == true) || (IsTimedOut() == true) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

// Called once after isFinished returns true
void AutonPath::End()
{
	robot->drivetrain->Stop();
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void AutonPath::Interrupted()
{
   End();
}
