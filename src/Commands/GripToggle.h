#ifndef GripToggle_H
#define GripToggle_H


#include "WPILib.h"

class GripToggle: public Command
{
private:
	int Type;
	bool Finished;

public:
	GripToggle(int t);
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
