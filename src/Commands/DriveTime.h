#ifndef DriveTime_H
#define DriveTime_H

#include "WPILib.h"

class DriveTime: public Command
{
private:
	Timer *timer;
	double Time;
	double Speed;
	bool Finished;
public:
	DriveTime(double S, double D);
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
