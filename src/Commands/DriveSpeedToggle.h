#ifndef DriveSpeedToggle_H
#define DriveSpeedToggle_H

#include "WPILib.h"

class DriveSpeedToggle: public Command
{
private:
	int mode;
	bool finished;
public:
	DriveSpeedToggle();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
