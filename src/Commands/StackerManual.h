#ifndef StackerManual_H
#define StackerManual_H

#include "WPILib.h"

class StackerManual: public Command
{
public:
	StackerManual();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
