#include "WristManual.h"
#include "Robot.h"
#include "oi.h"

WristManual::WristManual()
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires(Robot::arm);
}

// Called just before this Command runs the first time
void WristManual::Initialize()
{

}

// Called repeatedly when this Command is scheduled to run
void WristManual::Execute()
{
	if(robot->JsOperator->GetRawAxis(2) > .1)
	{
		robot->Wrist->Set(robot->JsOperator->GetRawAxis(2));
	}
	else if(robot->JsOperator->GetRawAxis(3) > .1)
	{
		robot->Wrist->Set(-(robot->JsOperator->GetRawAxis(3)));
	}
	else
	{
		robot->Wrist->Set(0);
	}
}

// Make this return true when this Command no longer needs to run execute()
bool WristManual::IsFinished()
{
	return false;
}

// Called once after isFinished returns true
void WristManual::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void WristManual::Interrupted()
{

}
