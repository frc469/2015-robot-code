#ifndef KillArmPID_H
#define KillArmPID_H


#include "WPILib.h"

class KillArmPID: public Command
{
private:
	bool finished;
public:
	KillArmPID();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
