#ifndef DriveForwardNoEncoders_H
#define DriveForwardNoEncoders_H

#include "WPILib.h"
#include "PIDAverage.h"
#include "PIDDifference.h"
#include "DummyPIDOutput.h"

class DriveForwardNoEncoders: public Command
{
private:
	bool Finished;
	PIDController *DriveStability;
	PIDController *SetPointDrive;
	PIDAverage1 *DistanceFromSetPoint;
	PIDDifference1 *DistanceFromEachother;
	DummyPIDOutput *AverageOutput;
	DummyPIDOutput *DifferenceOutput;
	Timer *TimeOut;
	double MaxSpeed;

public:
	double DriveDistance;
	DriveForwardNoEncoders(double D, double M);
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

};

#endif
