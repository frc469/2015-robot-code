#ifndef StackerPosition_H
#define StackerPosition_H

#include "WPILib.h"

class StackerPosition: public Command
{
private:
	bool Finished;
	int setPosition;
	int skip;
public:
	StackerPosition( int position );
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif

