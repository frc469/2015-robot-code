#include "Btn7.h"
#include "robot.h"


Btn7::Btn7()
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires(Robot::dummy);
}

// Called just before this Command runs the first time
void Btn7::Initialize()
{
	Finished = false;
}

// Called repeatedly when this Command is scheduled to run
void Btn7::Execute()
{
	if (robot->OperatorMode == 1)
	{
		robot->Btn7Mode1->Start();
	}
	else if (robot->OperatorMode == 2)
	{
		robot->Btn7Mode2->Start();
	}
	Finished = true;
}

// Make this return true when this Command no longer needs to run execute()
bool Btn7::IsFinished()
{

	return Finished;
}

// Called once after isFinished returns true
void Btn7::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void Btn7::Interrupted()
{

}
