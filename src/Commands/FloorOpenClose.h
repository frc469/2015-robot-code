#ifndef FloorOpenClose_H
#define FloorOpenClose_H

#include "WPILib.h"

class FloorOpenClose: public Command
{
private:
	int Type;

	bool finished;
public:

	FloorOpenClose(int t);
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
	enum { floorOpen=0, floorClose=1, floorToggle=2 };
};

#endif
