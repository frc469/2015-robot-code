#include "FloorLoadAutomated.h"

#include "StackerPosition.h"
#include "FloorOpenClose.h"
#include "StackOpenClose.h"
#include "FloorMotor.h"
#include "Griptoggle.h"
#include "Finisher.h"
#include "robot.h"
#include "../subsystems/Stacker.h"

FloorLoadAutomated::FloorLoadAutomated(int M)
{
	// A command group will require all of the subsystems that each member
	// would require.
	// e.g. if Command1 requires chassis, and Command2 requires arm,
	// a CommandGroup containing them would require both the chassis and the
	// arm.

	// Add Commands here:
	// e.g. AddSequential(new Command1());
	//      AddSequential(new Command2());
	// these will run in order.

	// To run multiple commands at the same time,
	// use AddParallel()
	// e.g. AddParallel(new Command1());
	//      AddSequential(new Command2());
	// Command1 and Command2 will run in parallel.
	if(M == 1)
	{

//		AddSequential( new StackerPosition( robot->stacker_load_2, true ) );
//		AddParallel( new StackOpenClose(1) );
		AddSequential( new StackerPosition( robot->stacker_load_1 ) );
		AddParallel( new FloorOpenClose(2) );
		AddParallel( new StackOpenClose( 2 ) );
		AddSequential( new WaitCommand( 0.10 ) );
		AddSequential( new StackerPosition( robot->stacker_clear_1 ) );
		AddSequential(new Finisher());
	}
	else if(M == 2)
	{

//		AddSequential( new StackerPosition( robot->stacker_load_2, true ) );
//		AddParallel( new StackOpenClose( 1 ) );
		AddSequential( new StackerPosition( robot->stacker_load_1 ) );
		AddParallel( new FloorOpenClose(2) );
		AddParallel( new StackOpenClose( 2 ) );
		AddSequential( new WaitCommand( 0.10 ) );
		AddSequential( new StackerPosition( robot->stacker_hard_top ) );
		AddSequential(new Finisher());
	}
	else if(M == 3)
	{	// wait for the jaws to open enough
//		AddParallel( new StackOpenClose(1) );
		AddSequential( new StackerPosition( robot->stacker_load_1 ) );
		AddParallel( new FloorOpenClose(2) );
		AddParallel( new StackOpenClose( 2 ) );
		AddSequential( new WaitCommand( 0.10 ) );
		AddSequential( new StackerPosition( robot->stacker_clear_1 ) );
		AddSequential(new Finisher());
	}
	else if(M == 4)
	{

//		AddSequential( new StackerPosition( robot->stacker_load_2, true ) );
//		AddParallel( new StackOpenClose(1) );
		AddSequential( new StackerPosition( robot->stacker_load_1 ) );
		AddParallel( new FloorOpenClose(2) );
		AddParallel( new StackOpenClose( 2 ) );
		AddSequential( new WaitCommand( 0.10 ) );
		AddSequential( new StackerPosition( robot->stacker_floor_clear ) );
		AddSequential(new Finisher());
	}
	else if(M == 5)
	{

//		AddSequential( new StackerPosition( robot->stacker_load_2, true ) );
//		AddParallel( new StackOpenClose(1) );
		AddSequential( new StackerPosition( robot->stacker_load_1 ) );
		AddParallel( new FloorOpenClose(2) );
		AddParallel( new StackOpenClose( 2 ) );
		AddSequential( new WaitCommand( 0.10 ) );
		AddSequential( new StackerPosition( robot->stacker_bedford_falls) );
		AddSequential( new GripToggle(1) );
		AddSequential( new StackerPosition( robot->stacker_bedford_falls2) );
		AddSequential( new GripToggle(2) );
		AddSequential(new Finisher());
	}
	else if(M == 6)
	{
//		AddParallel( new FloorOpenClose(2) );
//		AddSequential( new StackerPosition( robot->stacker_load_2, true ) );
//		AddParallel( new StackOpenClose( 1 ) );
//		AddSequential( new StackerPosition( robot->stacker_load_1 ) );
		AddParallel( new FloorOpenClose(2) );
		AddSequential( new StackOpenClose( 2 ) );
		AddSequential( new WaitCommand( 0.10 ) );
		AddSequential( new StackerPosition( (robot->stacker_hard_top) ) );
		AddSequential(new Finisher());
	}
	else if(M == 7)
	{

//		AddSequential( new StackerPosition( robot->stacker_load_2, true ) );
//		AddParallel( new StackOpenClose( 1 ) );
		AddParallel( new StackerPosition( robot->stacker_load_2 ) );
		AddParallel( new FloorOpenClose(2) );
		AddSequential( new StackOpenClose( 1 ) );
		AddSequential(new Finisher());
	}
	if(M == 8)
	{
//		AddSequential( new StackerPosition( robot->stacker_load_2, true ) );
//		AddParallel( new StackOpenClose(1) );
		AddSequential( new StackerPosition( robot->stacker_load_1 ) );
		AddParallel( new FloorOpenClose(2) );
		AddParallel( new StackOpenClose( 2 ) );
		AddSequential( new WaitCommand( 0.10 ) );
		AddSequential(new Finisher());
	}
	if(M == 9)
	{
		/*AddParallel( new FloorOpenClose(2) );
//		AddSequential( new StackerPosition( robot->stacker_load_2, true ) );
//		AddParallel( new StackOpenClose(1) );
		AddSequential( new StackerPosition( robot->stacker_load_1 ) );
		AddParallel( new StackOpenClose( 2 ) );*/
//		AddSequential( new WaitCommand( 0.10 ) );
		AddSequential( new StackerPosition( robot->stacker_clear_1 ) );
		AddSequential(new Finisher());
	}

}
