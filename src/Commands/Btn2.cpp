#include "Btn2.h"
#include "Robot.h"
#include "Commands/StackOpenClose.h"
#include "Commands/ArmSet.h"

Btn2::Btn2()
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires(Robot::dummy);
}

// Called just before this Command runs the first time
void Btn2::Initialize()
{
	Finished = false;
}

// Called repeatedly when this Command is scheduled to run
void Btn2::Execute()
{
	if (robot->OperatorMode == 1)
	{
		robot->Btn2Mode1->Start();
	}
	else if (robot->OperatorMode == 2)
	{
		robot->Btn2Mode2->Start();
	}

	Finished = true;
}

// Make this return true when this Command no longer needs to run execute()
bool Btn2::IsFinished()
{
	return Finished;
}

// Called once after isFinished returns true
void Btn2::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void Btn2::Interrupted()
{

}
