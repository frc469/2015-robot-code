#include <Commands/DriveManual.h>
#include "Robot.h"

DriveManual::DriveManual() {
	Requires(Robot::drivetrain);
}

// Called just before this Command runs the first time
void DriveManual::Initialize() {}

// Called repeatedly when this Command is scheduled to run
void DriveManual::Execute() {
	Robot::drivetrain->TankDrive(robot->JsDriver);
}

// Make this return true when this Command no longer needs to run execute()
bool DriveManual::IsFinished() {
	return false;
}

// Called once after isFinished returns true
void DriveManual::End() {
	Robot::drivetrain->Stop();
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void DriveManual::Interrupted() {
	End();
}
