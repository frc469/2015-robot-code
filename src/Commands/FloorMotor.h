#ifndef FloorMotor_H
#define FloorMotor_H

#include "WPILib.h"

class FloorMotor: public Command
{
private:
	void init( int inOutStopp, bool togglep );
	int inOutStop;
	bool toggle;
	bool finished;
public:
	FloorMotor();
	FloorMotor( int inOutStop, bool toggle );
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
	enum { floorIn=-1, floorOut=1, floorStop=0, floorRight=2, floorLeft=-2 };
};

#endif
