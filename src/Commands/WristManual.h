#ifndef WristManual_H
#define WristManual_H

#include "WPILib.h"

class WristManual: public Command
{
public:
	WristManual();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
