#include "Finisher.h"
#include "robot.h"

Finisher::Finisher()
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Finished = false;
}

// Called just before this Command runs the first time
void Finisher::Initialize()
{

}

// Called repeatedly when this Command is scheduled to run
void Finisher::Execute()
{
	robot->autoFinished = true;
	Finished = true;
}

// Make this return true when this Command no longer needs to run execute()
bool Finisher::IsFinished()
{
	return Finished;
}

// Called once after isFinished returns true
void Finisher::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void Finisher::Interrupted()
{

}
