#ifndef StackOpenClose_H
#define StackOpenClose_H

#include "WPILib.h"

class StackOpenClose: public Command
{
private:
	int Type;
	bool StackOpen;
	bool finished;
public:
	StackOpenClose( int t );
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
