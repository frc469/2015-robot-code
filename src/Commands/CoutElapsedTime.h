#ifndef CoutElapsedTime_H
#define CoutElapsedTime_H

#include "WPILib.h"

class CoutElapsedTime: public Command
{
private:
	const char *txt;
public:
	CoutElapsedTime( const char *s );
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
