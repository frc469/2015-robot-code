#include "BumpCheck.h"
#include "robot.h"
#include "FloorLoadAutomated.h"
#include "FloorOpenClose.h"
#include <iostream>

BumpCheck::BumpCheck(int t)
{
//	robot->FloorOpen = robot->FloorSolenoid->Get();
	Type = t;
	TimeOut = new Timer();
	Finished = false;
	FloorOpen = new FloorOpenClose(0);
	FloorAuto = new FloorLoadAutomated(t);
	Running = false;
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
}

// Called just before this Command runs the first time
void BumpCheck::Initialize()
{
	Finished = false;
	Running = false;
	robot->autoFinished = false;
	TimeOut->Reset();
	TimeOut->Start();
	FloorOpen->Start();

}

// Called repeatedly when this Command is scheduled to run
void BumpCheck::Execute()
{

	std::cout <<"Floor Open is " << robot->FloorOpen << "\n";
	if(TimeOut->HasPeriodPassed(1.25))
	{
		Finished = true;
		End();
	}
	if((robot->FloorFinished == true) && (robot->FloorOpen == false) && ((robot->RightLimitSwitch->Get() == true) && (robot->LeftLimitSwitch->Get()) == true ) && (Running == false) && (TimeOut->HasPeriodPassed(.1) == true))
	{
		TimeOut->Reset();
		FloorAuto->Start();
		Running = true;

	}
	if((robot->FloorFinished == true) && (robot->FloorOpen == true) && (TimeOut->HasPeriodPassed(.1) == true))
	{
		Finished = true;
		End();
	}
	if((robot->autoFinished == true) && (Running = true) && (TimeOut->HasPeriodPassed(.1) == true))
	{
		Finished = true;
		End();
	}


}

// Make this return true when this Command no longer needs to run execute()
bool BumpCheck::IsFinished()
{
	return Finished;
}

// Called once after isFinished returns true
void BumpCheck::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void BumpCheck::Interrupted()
{

}
