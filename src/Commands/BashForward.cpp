#include "BashForward.h"
#include "robot.h"

// This is a table of left/right drive speeds to play back a motion profile.

float driveProfileBF[] = {
		0.050	,	0.100	,
		0.100	,	0.200	,
		0.150	,	0.300	,
		0.200	,	0.400	,
		0.250	,	0.500	,
		0.300	,	0.600	,
		0.350	,	0.700	,
		0.400	,	0.800	,
		0.450	,	0.900	,
		0.500	,	1.000	,
		0.500	,	1.000	,
		0.500	,	1.000	,
		0.500	,	1.000	,
		0.500	,	1.000	,
		0.500	,   0.000	,
		0.500	,	0.000	,
		0.500	,	0.000	,
		0.500	,	0.000	,
		0.500	,	0.000	,
		0.500	,	0.000	,
		0.500	,	0.000	,
		0.500	,	0.000	,
		0.500	,	0.000	,
		0.500	,	0.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		1.000	,	1.000	,
		0.900   ,   0.900   ,
		0.800   ,   0.800   ,
		0.700   ,   0.700   ,
		0.600   ,   0.600   ,
		0.500   ,   0.500   ,
		0.400   ,   0.400   ,
		0.300   ,   0.300   ,
		0.200   ,   0.200   ,
		0.100   ,   0.100   ,
		0.000   ,   0.000
};

BashForward::BashForward()
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires(Robot::drivetrain);
	driveTimer = new Timer();
	bDone = false;
	motorIndex = 0;
}

// Called just before this Command runs the first time
void BashForward::Initialize()
{
	SetTimeout(3);
	bDone = false;
	motorIndex = 0;
	driveTimer->Reset();
	driveTimer->Start();
}

// Called repeatedly when this Command is scheduled to run
void BashForward::Execute()
{
	if ( motorIndex < (int) (sizeof(driveProfileBF) / sizeof(driveProfileBF[0])) )
	{
		if (driveTimer->Get() > motorIndex * 0.025)
		{
			robot->LeftDrive1->Set(-driveProfileBF[motorIndex]);
			robot->RightDrive1->Set(-driveProfileBF[motorIndex+1]);
			motorIndex += 2;
		}
	}
	else
	{
		bDone = true;
	}
}

// Make this return true when this Command no longer needs to run execute()
bool BashForward::IsFinished()
{
	if( (bDone == true) || (IsTimedOut() == true) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

// Called once after isFinished returns true
void BashForward::End()
{
	robot->drivetrain->Stop();
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void BashForward::Interrupted()
{
	   End();
}
