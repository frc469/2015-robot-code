#ifndef DriveTurn_H
#define DriveTurn_H

#include "WPILib.h"

class DriveTurn: public Command
{
public:
	DriveTurn(float A);
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
private:
	float GoalAngle;
	bool Finished;
};

#endif
