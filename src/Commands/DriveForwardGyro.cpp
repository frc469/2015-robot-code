#include "DriveForwardGyro.h"
#include "robot.h"
#include <iostream>
DriveForwardGyro::DriveForwardGyro(double distance, double max_speed, int gyro_set)
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires(Robot::drivetrain);
	TimeOut = new Timer;
	DriveDistance = distance;
	gyroAngle = new GyroSource(robot->gyro);
	DistanceFromSetPoint = new PIDAverage1(robot->RightDrive1, robot->RightDrive1, 0, 0, ENCODER_CONVERSION, ENCODER_CONVERSION);
	AverageOutput = new DummyPIDOutput();
	DifferenceOutput = new DummyPIDOutput();
	DriveStability = new PIDController(0.02, 0.0, 0.0, gyroAngle, DifferenceOutput, .02);
	DriveStability->SetAbsoluteTolerance(.4);
	gyro_setpoint = gyro_set;
	DriveStability->SetSetpoint(gyro_setpoint);
	DriveStability->Enable();
	SetPointDrive = new PIDController(0.18, 0, 0, DistanceFromSetPoint, AverageOutput, .02);
	SetPointDrive->SetSetpoint(distance);
	SetPointDrive->Enable();
	Finished = false;
	MaxSpeed = max_speed;
}

// Called just before this Command runs the first time
void DriveForwardGyro::Initialize()
{
	Finished = false;
///	robot->gyro->Reset();
	robot->RightDrive1->SetPosition(0);
	TimeOut->Reset();
	TimeOut->Start();
	DriveStability->Enable();
	if(DriveDistance != 0)
	{
		SetPointDrive->Enable();
	}
	else
	{
		SetPointDrive->Disable();
	}
}

// Called repeatedly when this Command is scheduled to run
void DriveForwardGyro::Execute()
{
	double Result;
	double gAngle;

	if(AverageOutput->GetPIDResult() >= MaxSpeed)
	{
		Result = MaxSpeed;
	}
	else if(AverageOutput->GetPIDResult() <= (-MaxSpeed))
	{
		Result = -MaxSpeed;
	}
	else
	{
		Result = AverageOutput->GetPIDResult();
	}


	gAngle = robot->gyro->GetAngle();
	if((((DriveDistance == 0)) && ((gyro_setpoint - gAngle) < 0.4) && ((gyro_setpoint - gAngle) > -0.4)))
		{
		robot->LeftDrive1->Set(0);
		robot->RightDrive1->Set(0);
		robot->RightDrive1->SetPosition(0);
		Finished = true;

		}
	else if( (DriveDistance != 0) &&
			 ((DistanceFromSetPoint->PIDGet() - DriveDistance) < 1.0) &&
			 ((DistanceFromSetPoint->PIDGet() - DriveDistance) > -1.0) )
	{
		robot->LeftDrive1->Set(0);
		robot->RightDrive1->Set(0);
		robot->RightDrive1->SetPosition(0);
		Finished = true;
	}
//	else if(robot->gyro->GetAngle() > 1)
//	{
//		robot->LeftDrive1->Set(-(Result - DifferenceOutput->GetPIDResult()));
//		robot->LeftDrive2->Set(-(Result - DifferenceOutput->GetPIDResult()));
//		robot->RightDrive1->Set(-Result);
//		robot->RightDrive2->Set(-Result);
//	}
//	else if(robot->gyro->GetAngle() < -1)
//	{
//		robot->LeftDrive1->Set(-Result);
//		robot->LeftDrive2->Set(-Result);
//		robot->RightDrive1->Set((-(Result + DifferenceOutput->GetPIDResult())));
//		robot->RightDrive2->Set((-(Result + DifferenceOutput->GetPIDResult())));
//	}
	else
	{
		/* pos is for */
		float left = -Result;
		float right = -Result;
		float angleEr = DifferenceOutput->GetPIDResult();

		if( DriveDistance == 0 )
		{
			if( (angleEr < 0) && (angleEr > -0.2) ) angleEr = -0.2;
			else if( (angleEr > 0) && (angleEr < 0.2) ) angleEr = 0.2;
		}

		left  -= angleEr;
		right += angleEr;

		robot->LeftDrive1->Set(left);
		robot->RightDrive1->Set(right);

#if DRIVE_FORWARD_DEBUG
	std::cout << "DriveDistance: " << DriveDistance << " DistanceFrom=" << DistanceFromSetPoint->PIDGet();
	std::cout << " Gyro Set " << gyro_setpoint << " gyro->GetAngle=" << gAngle << " angleEr=" << angleEr << " left=" << left << " right=" << right << "\n";
#endif
	}

}

// Make this return true when this Command no longer needs to run execute()
bool DriveForwardGyro::IsFinished()
{
	if((TimeOut->HasPeriodPassed(5.0) == true) || (Finished == true))
	{
		TimeOut->Stop();
		TimeOut->Reset();
		End();
std::cout << "DriveForwardGyro::IsFinished\n";
		return true;
	}
	else
	{
		return false;
	}

//	return false;
}

// Called once after isFinished returns true
void DriveForwardGyro::End()
{
	robot->LeftDrive1->SetPosition(0);
	robot->RightDrive1->SetPosition(0);
	DriveStability->Disable();
	SetPointDrive->Disable();
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void DriveForwardGyro::Interrupted()
{

}
