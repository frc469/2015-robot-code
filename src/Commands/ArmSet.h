#ifndef ArmSet_H
#define ArmSet_H


#include "WPILib.h"

class ArmSet: public Command
{
private:
	double GoalArm;
	double GoalWrist;
	Timer *ArmTimer;
	bool finished;
public:
	ArmSet(double A, double W);
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
