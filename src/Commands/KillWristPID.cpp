#include "KillWristPID.h"
#include "subsystems/arm.h"
#include "robot.h"

KillWristPID::KillWristPID()
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	finished = false;
}

// Called just before this Command runs the first time
void KillWristPID::Initialize()
{
	finished = false;
}

// Called repeatedly when this Command is scheduled to run
void KillWristPID::Execute()
{
	if(robot->arm->WristShiz->IsEnabled() == true)
	{
		robot->arm->WristShiz->Disable();
	}
	else
	{
		robot->arm->WristShiz->Enable();
	}
	finished = true;
}

// Make this return true when this Command no longer needs to run execute()
bool KillWristPID::IsFinished()
{
	return finished;
}

// Called once after isFinished returns true
void KillWristPID::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void KillWristPID::Interrupted()
{

}
