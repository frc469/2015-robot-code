#include "Btn8.h"
#include "robot.h"

Btn8::Btn8()
{
	Requires(Robot::dummy);
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
}

// Called just before this Command runs the first time
void Btn8::Initialize()
{
	Finished = false;
}

// Called repeatedly when this Command is scheduled to run
void Btn8::Execute()
{
	if (robot->OperatorMode == 1)
	{
		robot->Btn8Mode1->Start();
	}
	else if (robot->OperatorMode == 2)
	{
		robot->Btn8Mode2->Start();
	}
	Finished = true;
}

// Make this return true when this Command no longer needs to run execute()
bool Btn8::IsFinished()
{
	return Finished;
}

// Called once after isFinished returns true
void Btn8::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void Btn8::Interrupted()
{

}
