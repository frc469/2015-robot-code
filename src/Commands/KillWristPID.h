#ifndef KillWristPID_H
#define KillWristPID_H


#include "WPILib.h"

class KillWristPID: public Command
{
private:
	bool finished;
public:
	KillWristPID();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
