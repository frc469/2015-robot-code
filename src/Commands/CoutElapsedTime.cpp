#include "CoutElapsedTime.h"
#include "robot.h"
#include <iomanip>

CoutElapsedTime::CoutElapsedTime( const char *s )
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	txt = s;
}

// Called just before this Command runs the first time
void CoutElapsedTime::Initialize()
{
	std::cout << txt << ": time " << std::setprecision(6) << robot->AutonTimer.Get() << "\n";
}

// Called repeatedly when this Command is scheduled to run
void CoutElapsedTime::Execute()
{
}

// Make this return true when this Command no longer needs to run execute()
bool CoutElapsedTime::IsFinished()
{
	return true;
}

// Called once after isFinished returns true
void CoutElapsedTime::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void CoutElapsedTime::Interrupted()
{

}
