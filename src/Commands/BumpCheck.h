#ifndef BumpCheck_H
#define BumpCheck_H

#include "WPILib.h"
#include "FloorOpenClose.h"
#include "FloorLoadAutomated.h"

class BumpCheck: public Command
{
public:
	BumpCheck(int t);
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:
	FloorLoadAutomated *FloorAuto;
	FloorOpenClose *FloorOpen;
	Timer *TimeOut;
	bool Finished;
	int Type;
	bool Running;
	bool Running2;
};

#endif
