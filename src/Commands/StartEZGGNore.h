#ifndef StartEZGGNore_H
#define StartEZGGNore_H

#include "Commands/CommandGroup.h"
#include "WPILib.h"

class StartEZGGNore: public CommandGroup
{
public:
	StartEZGGNore(int t);
};

#endif
