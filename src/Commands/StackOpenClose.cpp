#include <Commands/StackOpenClose.h>
#include "robot.h"



StackOpenClose::StackOpenClose( int t )
{
	//Requires( Robot::stacker );
	Type = t;
	finished = false;
}

// Called just before this Command runs the first time
void StackOpenClose::Initialize()
{
	if(robot->StackGrab->Get() == DoubleSolenoid::kReverse)
	{
		StackOpen = false;
	}
	else
	{
		StackOpen = true;
	}
	finished = false;
}

// Called repeatedly when this Command is scheduled to run
void StackOpenClose::Execute()
{
	if(Type == 0)
	{

		if(StackOpen == true)
		{
			robot->StackGrab->Set(DoubleSolenoid::kReverse);
			StackOpen = false;
		}
		else
		{
			robot->StackGrab->Set(DoubleSolenoid::kForward);
			StackOpen = true;
		}
	}
	else if(Type == 1)
	{
		robot->StackGrab->Set(DoubleSolenoid::kForward);
		StackOpen = true;
	}
	else if(Type == 2)
	{
		robot->StackGrab->Set(DoubleSolenoid::kReverse);
		StackOpen = false;
	}
	finished = true;
}

// Make this return true when this Command no longer needs to run execute()
bool StackOpenClose::IsFinished()
{
	return finished;
}

// Called once after isFinished returns true
void StackOpenClose::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void StackOpenClose::Interrupted()
{

}
