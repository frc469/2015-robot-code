#ifndef Btn7_H
#define Btn7_H


#include "WPILib.h"

class Btn7: public Command
{
private:
	bool Finished;
public:
	Btn7();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
