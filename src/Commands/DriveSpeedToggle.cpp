#include "DriveSpeedToggle.h"
#include "robot.h"
#include <iostream>

DriveSpeedToggle::DriveSpeedToggle()
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	mode = 0;
	robot->driverJoystickDivider = 1.0;
}

// Called just before this Command runs the first time
void DriveSpeedToggle::Initialize()
{
	finished = false;
}

// Called repeatedly when this Command is scheduled to run
void DriveSpeedToggle::Execute()
{
	if( mode == 1 )
	{
		std::cout <<"Ramp rate is 14" << "/n";
		robot->LeftDrive1->SetVoltageRampRate(14);
		robot->RightDrive1->SetVoltageRampRate(14);
		robot->driverJoystickDivider = 1.0;
		mode = 0;
	}
	else
	{
		std::cout <<"Ramp rate is 28" << "/n";
		robot->LeftDrive1->SetVoltageRampRate(28);
		robot->RightDrive1->SetVoltageRampRate(28);
		robot->driverJoystickDivider = 1.0;
		mode = 1;
	}
	finished = true;
}

// Make this return true when this Command no longer needs to run execute()
bool DriveSpeedToggle::IsFinished()
{
	return finished;
}

// Called once after isFinished returns true
void DriveSpeedToggle::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void DriveSpeedToggle::Interrupted()
{

}
