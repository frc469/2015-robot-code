#include "FireOJ.h"
#include "robot.h"


FireOJ::FireOJ(int t)
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires(Robot::cansteal);
	Type = t;
	Finished = false;

}

// Called just before this Command runs the first time
void FireOJ::Initialize()
{
//	robot->StateOfTheOJ = robot->OJ->Get();
}

// Called repeatedly when this Command is scheduled to run
void FireOJ::Execute()
{
	if(Type == 0)
	{
		if(robot->StateOfTheOJ == true)
		{
			robot->OJ->Set(DoubleSolenoid::kReverse);
			robot->OJRight->Set(DoubleSolenoid::kReverse);
			robot->StateOfTheOJ = false;
		}
		else
		{
			robot->OJ->Set(DoubleSolenoid::kForward);
			robot->OJRight->Set(DoubleSolenoid::kForward);
			robot->StateOfTheOJ = true;
		}
	}
	if(Type == 1)
	{
		robot->OJ->Set(DoubleSolenoid::kForward);
		robot->OJRight->Set(DoubleSolenoid::kForward);
		robot->StateOfTheOJ = true;
	}
	if(Type == 2)
	{
		robot->OJ->Set(DoubleSolenoid::kReverse);
		robot->OJRight->Set(DoubleSolenoid::kReverse);
		robot->StateOfTheOJ = false;
	}
	Finished = true;

#if ROBOT_TYPE
	if(Type == 0)
	{
		if(robot->StateOfTheOJ == true)
		{
			robot->OJ->Set(false);
			robot->OJRight->Set(true);
			robot->StateOfTheOJ = false;
		}
		else
		{
			robot->OJ->Set(true);
			robot->OJRight->Set(false);
			robot->StateOfTheOJ = true;
		}
	}
	if(Type == 1)
	{
		robot->OJ->Set(true);
		robot->OJRight->Set(true);
		robot->StateOfTheOJ = true;
	}
	if(Type == 2)
	{
		robot->OJ->Set(false);
		robot->OJRight->Set(false);
		robot->StateOfTheOJ = false;
	}
	Finished = true;
#endif
}

// Make this return true when this Command no longer needs to run execute()
bool FireOJ::IsFinished()
{
	return Finished;
}

// Called once after isFinished returns true
void FireOJ::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void FireOJ::Interrupted()
{

}
