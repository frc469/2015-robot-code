#ifndef ZeroArm_H
#define ZeroArm_H

#include "WPILib.h"

class ZeroArm: public Command
{
public:
	ZeroArm();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
