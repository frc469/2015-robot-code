#ifndef FireOJ_H
#define FireOJ_H

#include "WPILib.h"

class FireOJ: public Command
{
public:
	FireOJ(int t);
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
private:

	int Type;
	bool Finished;
};

#endif
