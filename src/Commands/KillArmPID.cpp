#include "KillArmPID.h"
#include "subsystems/arm.h"
#include "robot.h"

KillArmPID::KillArmPID()
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	finished = false;
}

// Called just before this Command runs the first time
void KillArmPID::Initialize()
{
	finished = false;
}

// Called repeatedly when this Command is scheduled to run
void KillArmPID::Execute()
{
	if(robot->arm->ArmAverage->IsEnabled() == true)
	{
		robot->arm->ArmAverage->Disable();
	}
	else
	{
		robot->arm->ArmAverage->Enable();
	}
	finished = true;
}

// Make this return true when this Command no longer needs to run execute()
bool KillArmPID::IsFinished()
{
	return finished;
}

// Called once after isFinished returns true
void KillArmPID::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void KillArmPID::Interrupted()
{

}
