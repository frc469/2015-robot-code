#include <Commands/StackerManual.h>
#include "robot.h"
#include "subsystems/stacker.h"
#include "Commands/FloorLoadAutomated.h"

StackerManual::StackerManual()
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires( Robot::stacker );
}

// Called just before this Command runs the first time
void StackerManual::Initialize()
{
}

// Called repeatedly when this Command is scheduled to run
void StackerManual::Execute()
{
	double up, down;
	double position;
	double motor;

	position = robot->Stack->GetPosition();

	up = robot->JsDriver->GetRawAxis(3);
	down = robot->JsDriver->GetRawAxis(2);

	if ((up >= 0.15) && (position < robot->stacker_hard_top) )
	{
		motor = 1 * up;
	}
	else if( (down >= 0.15) && (position > robot->stacker_hard_bottom))
	{
		motor = -1 * down;
	}
	else if((robot->JsOperator->GetRawAxis(5) >= .15) && (position < robot->stacker_hard_top))
	{
		motor = robot->JsOperator->GetRawAxis(5);
	}
	else if((robot->JsOperator->GetRawAxis(5) <= -.15) && (position > robot->stacker_hard_bottom))
	{
		motor = robot->JsOperator->GetRawAxis(5);
	}
	else
	{
		// joystick released
		motor = 0;
	}
	robot->stacker->StackerOutput( motor );
}

// Make this return true when this Command no longer needs to run execute()
bool StackerManual::IsFinished()
{
	return false;
}

// Called once after isFinished returns true
void StackerManual::End()
{
	robot->stacker->StackerOutput( 0 );
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void StackerManual::Interrupted()
{
	End();
}
