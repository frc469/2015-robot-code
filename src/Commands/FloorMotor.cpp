#include "FloorMotor.h"
#include "robot.h"

void FloorMotor::init( int inOutStopp, bool togglep )
{
	Requires(Robot::floorload);
	inOutStop = inOutStopp;
	toggle = togglep;
}

FloorMotor::FloorMotor( int inOutStop, bool toggle )
{
	init( inOutStop, toggle );
	finished = false;
}

// Called just before this Command runs the first time
void FloorMotor::Initialize()
{
	finished = false;
}

// Called repeatedly when this Command is scheduled to run
void FloorMotor::Execute()
{
	if( inOutStop == floorIn )
	{
		if( (toggle = true) && (robot->LeftFloor->Get() != 1) )
		{
			robot->LeftFloor->Set( 1 );
			robot->RightFloor->Set( -1 );
		}
		else
		{
			robot->LeftFloor->Set( 0 );
			robot->RightFloor->Set( 0 );
		}
	}
	else if( inOutStop == floorOut )
	{
		if( (toggle == true) && (robot->LeftFloor->Get() != -1) )
		{
			robot->LeftFloor->Set( -1 );
			robot->RightFloor->Set( 1 );
		}
		else
		{
			robot->LeftFloor->Set( 0 );
			robot->RightFloor->Set( 0 );
		}
	}

	else if ( inOutStop == floorRight )
	{
		robot->LeftFloor->Set( 0 );
		robot->RightFloor->Set( 1 );
	}

	else if ( inOutStop == floorLeft )
	{
		robot->LeftFloor->Set( -1 );
		robot->RightFloor->Set( 0 );
	}

	else if( inOutStop == floorStop )
	{
		robot->LeftFloor->Set( 0 );
		robot->RightFloor->Set( 0 );
	}

	else
	{
		robot->LeftFloor->Set( 0 );
		robot->RightFloor->Set( 0 );
	}
	finished = true;
}

// Make this return true when this Command no longer needs to run execute()
bool FloorMotor::IsFinished()
{
	return finished;
}

// Called once after isFinished returns true
void FloorMotor::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void FloorMotor::Interrupted()
{

}
