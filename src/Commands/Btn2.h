#ifndef Btn2_H
#define Btn2_H

#include "WPILib.h"

class Btn2: public Command
{
public:
	Btn2();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
private:
	bool Finished;
};

#endif
