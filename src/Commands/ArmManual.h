#ifndef ArmManual_H
#define ArmManual_H

#include "WPILib.h"
#include "subsystems/arm.h"

class ArmManual: public Command
{
private:
	bool FirstManual;
	bool FirstPID;
	int Type;
	Timer ManualRelease;
	float motorSetting;
	float pidLeft;
	float pidRight;

public:
	ArmManual(int S);
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
