#include "DriveTurn.h"
#include "Robot.h"

DriveTurn::DriveTurn(float A)
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires(Robot::drivetrain);
	GoalAngle = A;
	Finished = false;
}

// Called just before this Command runs the first time
void DriveTurn::Initialize()
{
	Finished = false;
	robot->gyro->Reset();
}

// Called repeatedly when this Command is scheduled to run
void DriveTurn::Execute()
{
	if (robot->gyro->GetAngle() < GoalAngle)
	{
		robot->LeftDrive1->Set(0.5);
		robot->RightDrive1->Set(-.5);
	}
	else if (robot->gyro->GetAngle() >= GoalAngle)
	{
		Finished = true;
	}
}

// Make this return true when this Command no longer needs to run execute()
bool DriveTurn::IsFinished()
{
	return Finished;
}

// Called once after isFinished returns true
void DriveTurn::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void DriveTurn::Interrupted()
{

}
