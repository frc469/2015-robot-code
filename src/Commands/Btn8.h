#ifndef Btn8_H
#define Btn8_H


#include "WPILib.h"

class Btn8: public Command
{
private:
	bool Finished;

public:
	Btn8();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
