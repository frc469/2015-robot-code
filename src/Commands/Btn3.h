#ifndef Btn3_H
#define Btn3_H

#include "WPILib.h"

class Btn3: public Command
{
public:
	Btn3();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
private:
	bool Finished;
};

#endif
