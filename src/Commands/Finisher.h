#ifndef Finisher_H
#define Finisher_H


#include "WPILib.h"

class Finisher: public Command
{
public:
	Finisher();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:
	bool Finished;
};

#endif
