#include "ArmSet.h"
#include "robot.h"
#include "subsystems/arm.h"

ArmSet::ArmSet(double A, double W)
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires(Robot::arm);

	ArmTimer = new Timer;
	GoalArm = A;
	GoalWrist = W;
	finished = false;
}

// Called just before this Command runs the first time
void ArmSet::Initialize()
{

	ArmTimer->Reset();
	finished = false;

}

// Called repeatedly when this Command is scheduled to run
void ArmSet::Execute()
{

	ArmTimer->Start();
	if(GoalWrist != 0)
	{
		robot->arm->WristGoal = GoalWrist;
	}
	if(ArmTimer->HasPeriodPassed(.1))
	{
		if( GoalArm < 300.0)
		{
			robot->arm->ArmAverage->SetSetpoint(GoalArm);
		}
		finished = true;
	}
		//std::cout << " Goal Position " << GoalArm << "\n";
}

// Make this return true when this Command no longer needs to run execute()
bool ArmSet::IsFinished()
{
	return finished;
}

// Called once after isFinished returns true
void ArmSet::End()
{

}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void ArmSet::Interrupted()
{

}
