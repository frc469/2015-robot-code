#ifndef ShiftGear_H
#define ShiftGear_H

#include "WPILib.h"

class ShiftGear: public Command
{
private:
	bool finished;
	int shift_state;
	bool shift_toggle;
public:
	ShiftGear();
	ShiftGear( int state );
	void init( int state, bool toggle );
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
