#ifndef DriveWaitDrive_H
#define DriveWaitDrive_H

#include "Commands/CommandGroup.h"
#include "WPILib.h"

class DriveWaitDrive: public CommandGroup
{
public:
	DriveWaitDrive();
};

#endif
