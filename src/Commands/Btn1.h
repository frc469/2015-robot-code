#ifndef Btn1_H
#define Btn1_H

#include "WPILib.h"

class Btn1: public Command
{
public:
	Btn1();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
private:
	bool Finished;
};

#endif
