#ifndef AutonCurrentScan_H
#define AutonCurrentScan_H

#include "WPILib.h"

class AutonCurrentScan: public Command
{
public:
	AutonCurrentScan(bool Y);
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
	bool scan;
};

#endif
