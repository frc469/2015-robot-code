#include "DriveTime.h"
#include "robot.h"

DriveTime::DriveTime(double S, double T)
{
	// Use Requires() here to declare subsystem dependencies
	// eg. Requires(chassis);
	Requires(robot->drivetrain);
	timer = new Timer;
	Speed = S;
	Time = T;
	Finished = false;
}

// Called just before this Command runs the first time
void DriveTime::Initialize()
{
	timer->Reset();
	timer->Start();
	Finished = false;
}

// Called repeatedly when this Command is scheduled to run
void DriveTime::Execute()
{

	if(timer->HasPeriodPassed(Time))
	{
		robot->LeftDrive1->Set(0);
		robot->RightDrive1->Set(0);
		End();
		Finished = true;
	}
	else
	{
		robot->LeftDrive1->Set(-Speed);
		robot->RightDrive1->Set(-Speed);
	}
}

// Make this return true when this Command no longer needs to run execute()
bool DriveTime::IsFinished()
{
	return Finished;
}

// Called once after isFinished returns true
void DriveTime::End()
{
	robot->LeftDrive1->Set(0);
	robot->RightDrive1->Set(0);
}

// Called when another command which requires one or more of the same
// subsystems is scheduled to run
void DriveTime::Interrupted()
{

}
