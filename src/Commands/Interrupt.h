#ifndef Interrupt_H
#define Interrupt_H


#include "WPILib.h"

class Interrupt: public Command
{
public:
	Interrupt();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
