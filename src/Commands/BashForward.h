#ifndef BashForward_H
#define BashForward_H

#include "WPILib.h"

class BashForward: public Command
{
public:
	BashForward();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:
	int motorIndex;
	Timer *driveTimer;
	bool bDone;
};

#endif
