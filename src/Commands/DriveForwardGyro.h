#ifndef DriveForwardGyro_H
#define DriveForwardGyro_H

#include "WPILib.h"
#include "PIDAverage.h"
#include "PIDDifference.h"
#include "DummyPIDOutput.h"
#include "gyrosource.h"

class DriveForwardGyro: public Command
{
private:
	GyroSource *gyroAngle;
	bool Finished;
	PIDController *DriveStability;
	PIDController *SetPointDrive;
	PIDAverage1 *DistanceFromSetPoint;
	DummyPIDOutput *AverageOutput;
	DummyPIDOutput *DifferenceOutput;
	Timer *TimeOut;
	double MaxSpeed;
	double gyro_setpoint;
public:
	double DriveDistance;
	DriveForwardGyro(double distance, double max_speed, int gyro_set);
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
