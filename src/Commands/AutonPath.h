#ifndef AutonPath_H
#define AutonPath_H

#include "WPILib.h"

class AutonPath: public Command
{
public:
	AutonPath();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();

private:
	int motorIndex;
	Timer *driveTimer;
	bool bDone;
};

#endif
