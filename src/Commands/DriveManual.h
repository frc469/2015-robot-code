#ifndef DriveManual_H
#define DriveManual_H

#include "WPILib.h"
#include "subsystems/DriveTrain.h"

/**
 * This command allows a joystick to drive the robot. It is always running
 * except when interrupted by another command.
 */
class DriveManual: public Command {
public:
	DriveManual();
	void Initialize();
	void Execute();
	bool IsFinished();
	void End();
	void Interrupted();
};

#endif
