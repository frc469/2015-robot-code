#include "WPILib.h"
#include <iostream>
#include "Robot.h"
#include "math.h"
#include "Commands/DriveForward.h"
#include "Commands/FloorOpenClose.h"
#include "Commands/ArmSet.h"
#include "Commands/ShiftGear.h"
#include "RobotSettings.h"
#include "Commands/ThreeToteAuton.h"
#include "Commands/DoNothing.h"
#include "Commands/GrabCanWithArmAuton.h"
#include "Commands/DriveSpeedToggle.h"
#include "commands/DriveWaitDrive.h"
#include "commands/autonpath.h"
#include "commands/BashForward.h"
#include "commands/StartEZGGnore.h"
#include "commands/OneCanFromMiddle.h"
#include "commands/DriveForwardGyro.h"
#include "Commands/FireOJ.h"
#include "Commands/WinchCanSteal.h"
#include "Commands/OJ.h"
#include "Commands/ArmSet.h"

DriveTrain *Robot::drivetrain = NULL;
Arm *Robot::arm = NULL;
CanSteal *Robot::cansteal = NULL;
Floor *Robot::floorload = NULL;
Stacker *Robot::stacker = NULL;
OI* Robot::oi = NULL;
Dummy* Robot::dummy = NULL;



void Robot::RobotInit()
{
	try
	{
		//lw = LiveWindow::GetInstance();

	//	SmartDashboard::init();
		//Members in da club
		Test1 = 0;
		p = 80;
		i = 0.05;
		d = 1;
		izone = 300;
		ramprate = 48;
		profile = 1;
		gearHigh = true;
		floorClosed = false;
		DriverPrevPOV = 0;
		ControllerDPAD = 0;
		LogCount = 0;
		JsDriver = new Joystick(0);
		JsOperator = new Joystick(1);
		LeftDrive1 = new CANTalonSafe(1);
		RightDrive1 = new CANTalonSafe(3);
		LeftFloor = new CANTalonSafe(5);
		RightFloor = new CANTalonSafe(6);
		LeftShoulder = new CANTalonSafe(7);
		RightShoulder = new CANTalonSafe(8);
		Wrist = new CANTalonSafe(9);
		Stack = new CANTalonSafe(10);
		CanStealMotor = new CANTalonSafe(11);
		Drive = new Solenoid(0, 1);//good
		FloorSolenoid = new Solenoid(1, 5);//good
		AutonLoops = 0;
		OJ = new DoubleSolenoid(1, 4, 3);//good
		OJRight = new DoubleSolenoid(0, 3, 4);
#if ROBOT_TYPE
		OJ = new Solenoid(1, 4);//good
		OJRight = new Solenoid(0, 3);
#endif
		StackGrab = new DoubleSolenoid(0, 5, 2);
		CanGrab = new DoubleSolenoid(0, 0, 6);
		gyro = new Gyro(1);
		OperatorMode = 1;
		driverJoystickDivider = 1.0;
	    left_arm_pot = 534;
	    right_arm_pot = 626;
	    wrist_pot = 558;
	    stacker_hard_top = 800;
	    Auton = 0;
	    RobotSettings();
		stacker_clear_2 = stacker_hard_top - 160;
		stacker_clear_1 = stacker_hard_top - 310;
		stacker_load_2 = stacker_hard_top - 430;
		stacker_floor_clear = stacker_hard_top - 600;
		stacker_load_1 = stacker_hard_top - 660;
		stacker_hard_bottom = stacker_hard_top - 660;
		stacker_bedford_falls = stacker_hard_top - 430;
		stacker_bedford_falls2 = stacker_hard_top - 110;
		Stack->ConfigSoftPositionLimits( stacker_hard_top, stacker_hard_bottom );
		Wrist->ConfigSoftPositionLimits((wrist_pot + 100), (wrist_pot - 100)); //!*!*!* 333 for 3-turn
		CheckPower = new ControllerPower;
		LeftLimitSwitch = new DigitalInput(1);
		RightLimitSwitch = new DigitalInput(2);
		FloorOpen = false;
		autoFinished = false;
		Initialize = false;
		DisableTime = new Timer();
		StateOfTheOJ = false;

		// burn some time until the CANtalons respond
		Wait(.05);

		drivetrain = new DriveTrain();
		arm = new Arm();
		cansteal = new CanSteal();
		floorload = new Floor();
		stacker = new Stacker();
		dummy = new Dummy();
		oi = new OI();
		DrivePOV = new ShiftGear();
		BtnDriveSpeedToggle = new DriveSpeedToggle();
		ToggleOJ = new FireOJ(0);
		WinchSteal = new WinchCanSteal();
		WristSet90 = new ArmSet(469, -90);
		WristSet180 = new ArmSet(469, -180);
#if ROBOT_TYPE
		OJ->Set(DoubleSolenoid::kForward);
		OJRight->Set(DoubleSolenoid::kForward);
#endif


//		autoChooser = new SendableChooser();
//		autoChooser->AddDefault("Do Nothing", new DoNothing());
//		autoChooser->AddObject("Rise Grab Go ", new StartEZGGNore(1));
//		autoChooser->AddObject("Grab Go ", new StartEZGGNore(2));
//		autoChooser->AddObject("CanGrab With Arm", new OneCanFromMiddle());
//		autoChooser->AddObject("OJ", new OJ1());
//		autoChooser->AddObject("AutonPath", new AutonPath());
		//autoChooser->AddObject("TestDrive", new DriveWaitDrive());
//		autoChooser->AddObject("Three tote", new ThreeToteAuton());
//		autoChooser->AddObject("Drive Forward", new DriveForwardGyro(50,.76, 0));
//		autoChooser->AddObject("Bash Forward", new BashForward());
		//autoChooser->AddObject( "CanSteal", new CanSteal());


//		SmartDashboard::PutData("Auto Mode", autoChooser);

		DriveSpeed = (1-(CheckPower->GetInputVoltage()/42));

		std::cout << "Robot Initialized, build " << __DATE__ << " " << __TIME__ << "\n";
	}
	catch(int e)
	{
		std::cout << "RIP: " << e << "\n";
	}

}

void Robot::AutonomousInit()
{
	if(Auton == 7)
	{
		OJ->Set(DoubleSolenoid::kForward);
		OJRight->Set(DoubleSolenoid::kForward);
#if ROBOT_TYPE
		OJ->Set(true);
		OJRight->Set(true);
#endif
	}
//    RobotSettings();
	stacker_clear_2 = stacker_hard_top - 160;
	stacker_clear_1 = stacker_hard_top - 320;
	stacker_load_2 = stacker_hard_top - 430;
	stacker_floor_clear = stacker_hard_top - 600;
	stacker_load_1 = stacker_hard_top - 660;
	stacker_hard_bottom = stacker_hard_top - 660;
	stacker_bedford_falls = stacker_hard_top - 430;
	stacker_bedford_falls2 = stacker_hard_top - 110;
	Stack->ConfigSoftPositionLimits( stacker_hard_top, stacker_hard_bottom );
	Wrist->ConfigSoftPositionLimits((wrist_pot + 300), (wrist_pot - 300));
	Scheduler::GetInstance()->RemoveAll();
	TalonInit(0);
	gyro->Reset();
	arm->Initialized = false;
	LeftDrive1->SetPosition(0);
	RightDrive1->SetPosition(0);
	arm->PIDInitializer->Reset();
	switch((Auton))
	{
		case 0:
		default:
			autonomousCommand = new DoNothing();
			break;
		case 1:
			autonomousCommand = new StartEZGGNore(1);
			break;
		case 2:
			autonomousCommand = new StartEZGGNore(2);
			break;
		case 3:
			autonomousCommand = new OJ1(1);
			break;
		case 4:
			autonomousCommand = new ThreeToteAuton();
			break;
		case 5:
			autonomousCommand = new DriveForwardGyro(0,.76, 30);
			break;
		case 6:
			autonomousCommand = new OJ1(2);
			break;
		case 7:
			autonomousCommand = new OJ1(3);
			break;
	}
			//		autoChooser = new SendableChooser();
			//		autoChooser->AddDefault("Do Nothing", new DoNothing());
			//		autoChooser->AddObject("Rise Grab Go ", new StartEZGGNore(1));
			//		autoChooser->AddObject("Grab Go ", new StartEZGGNore(2));
			//		autoChooser->AddObject("CanGrab With Arm", new OneCanFromMiddle());
			//		autoChooser->AddObject("OJ", new OJ1());
			//		autoChooser->AddObject("AutonPath", new AutonPath());
					//autoChooser->AddObject("TestDrive", new DriveWaitDrive());
			//		autoChooser->AddObject("Three tote", new ThreeToteAuton());
			//		autoChooser->AddObject("Drive Forward", new DriveForwardGyro(50,.76, 0));
			//		autoChooser->AddObject("Bash Forward", new BashForward());
					//autoChooser->AddObject( "CanSteal", new CanSteal());


//	autonomousCommand = (Command*) autoChooser->GetSelected();
	AutonTimer.Reset();
	AutonTimer.Start();
	autonomousCommand->Start();
}

void Robot::AutonomousPeriodic()
{
	if ( AutonLoops < 25)
	{
			AutonLoops++;
	}
	else
	{
		std::cout << " Gyro Angle is " << (int) gyro->GetAngle() << "\n";
		std::cout << " Arm Angle is " << arm->RightArm << "\n";
		AutonLoops = 0;
	}
	if( autonCurrentScan == true )
	{
		// Make sure the arm didn't smash the first can
		// - if either side is drawing more than 4 amps, shut everything down!
		if( (robot->arm->armGoal - robot->arm->armPosition) > 5 || (robot->arm->armGoal - robot->arm->armPosition) < -5)
		{
			EMERGENCY_STOP();
		}
	}
//	SmartDashboard::PutData("Auto Mode", autoChooser);

	try
	{
		Scheduler::GetInstance()->Run();
	}
	catch( int e )
	{
	    std::cout << "Exception: " << e << "\n";
	}
	arm->Periodic();
	Log();

}

void Robot::TeleopInit()
{
	Scheduler::GetInstance()->RemoveAll();
	TalonInit(1);
    RobotSettings();
	stacker_clear_2 = stacker_hard_top - 160;
	stacker_clear_1 = stacker_hard_top - 340;
	stacker_load_2 = stacker_hard_top - 430;
	stacker_floor_clear = stacker_hard_top - 600;
	stacker_load_1 = stacker_hard_top - 660;
	stacker_hard_bottom = stacker_hard_top - 660;
	stacker_bedford_falls = stacker_hard_top - 430;
	stacker_bedford_falls2 = stacker_hard_top - 110;
	Stack->ConfigSoftPositionLimits( stacker_hard_top, stacker_hard_bottom );
	Wrist->ConfigSoftPositionLimits((wrist_pot + 300), (wrist_pot - 300));
	LeftDrive1->SetPosition(0);
	RightDrive1->SetPosition(0);
	arm->Initialized = false;
	arm->PIDInitializer->Reset();
	gyro->Reset();
//	SmartDashboard::PutData("Auto Mode", autoChooser);
}

void Robot::TeleopPeriodic()
{
	int pov;
//	SmartDashboard::PutData("Auto Mode", autoChooser);
	try
	{
		Scheduler::GetInstance()->Run();
	}
	catch( int e )
	{
	    std::cout << "Exception: " << e << "\n";
	}

	if( emergencyStop == true )
	{
	   EMERGENCY_STOP();
	   emergencyStop = false;
	}

	arm->Periodic();

	// driver shift
	pov = JsDriver->GetPOV();
	if( pov == 180 )
	{
		if( DriverPrevPOV != 180 )
		{
			DrivePOV->Start();
		}
		DriverPrevPOV = 180;
	}
	else if( pov == 0 )
	{
		if( DriverPrevPOV != 0 )
		{
			robot->Btn2Mode1->Start();
		}
		DriverPrevPOV = 0;
	}
	else if( pov == 90)
	{
		if( DriverPrevPOV != 90 )
		{
		ToggleOJ->Start();
		}
		DriverPrevPOV = 90;
	}
	else if( pov == 270)
	{
			WinchSteal->Start();
	}
	else
	{
		DriverPrevPOV = -1;

	}

	// operator modes
	pov = JsOperator->GetPOV();
	if( pov == 0 )
	{
		if(OpPrevPOV != 0)
		{
			WristSet90->Start();

		}
		OpPrevPOV = 0;
	}
	else if( pov == 180 )
	{
		if(OpPrevPOV != 180)
		{
			WristSet180->Start();
		}
		OpPrevPOV = 180;
	}

	Log();
}

void Robot::TestPeriodic()
{
	lw->Run();
}

void Robot::DisabledInit()
{
#if ROBOT_TYPE
#else
	OJ->Set(DoubleSolenoid::kReverse);
	OJRight->Set(DoubleSolenoid::kReverse);
#endif
	arm->ArmAverage->Disable();
	arm->WristShiz->Disable();
	LeftDrive1->SetPosition(0);
	RightDrive1->SetPosition(0);
	Drive->Set(true);
	FloorSolenoid->Set(false);
	DisableTime->Reset();
	DisableTime->Start();
	Initialize = false;
}

void Robot::DisabledPeriodic()
{

//	Test1++;
//	SmartDashboard::PutNumber("Test 2", Test1);
	if ( AutonLoops < 25)
	{
			AutonLoops++;
	}
	else
	{
		std::cout << " Gyro Angle is " << (int) gyro->GetAngle() << "\n";
		std::cout << " Arm Angle is " << arm->RightArm << "\n";
		AutonLoops = 0;
	}
	if((DisableTime->HasPeriodPassed(1) == true) && (Initialize == false))
	{
	TalonInit(0);
    RobotSettings();
    std::cout <<"Initialized in Disable" << "\n";
    Initialize = true;

	}
	}


void Robot::Log()
{
	//std::cout << "Stacker Position " << Stack->GetPosition();
//	std::cout << "Stacker Draw " << Stack->GetOutputCurrent() << "\n";
	//Das Log
	if(LogCount < 5)
	{
		++LogCount;
	}
	else if(LogCount == 5)
	{
//		std::cout << "Right Limit " << RightLimitSwitch->Get() << "Left Limit " << LeftLimitSwitch->Get() << "\n";
//		std::cout << "Left Drive is " << LeftDrive1->GetPosition() << " Right Drive is " << RightDrive1->GetPosition() <<  " Robot Angle" << gyro->GetAngle() << "\n";
	//	std::cout << "Gyro Angle " << gyro->GetAngle() << "\n";
	//			std::cout << "Right Drive is " << ENCODER_CONVERSION*RightDrive1->GetPosition() <<"\n";
	//			std::cout << "Left Drive Setting is " << LeftDrive1->Get() << "Right Drive Setting is " << RightDrive1->Get() <<"\n";
	//			std::cout << "Stack Position is " << Stack->GetAnalogIn() << "\n";
				//std::cout << "Left Shoulder is " << LeftShoulder->GetAnalogIn() << "\n";
				//std::cout << "Right Shoulder is " << RightShoulder->GetAnalogIn() << "\n";
		//std::cout <<"Robot Angle" << gyro->GetAngle() << "\n";

				LogCount = 0;
	}
	//std::cout << "Left Drive is " << LeftDrive1->GetPosition() << " Right Drive is " << RightDrive1->GetPosition() <<  " Robot Angle" << gyro->GetAngle() << "\n";
	//std::cout << "Left Drive Setting is " << LeftDrive1->Get() << "Right Drive Setting is " << RightDrive1->Get() <<"\n";
//	std::cout <<"Floor Load Speed " <<LeftFloor->Get() << RightFloor->Get() <<"\n";

}

//#define START_ROBOT_sASS(_ClassName_)

Robot *robot;

void Robot::TalonInit(int M)
{
	if(M == 0)
	{
		LeftDrive1->SetVoltageRampRate(0);
		RightDrive1->SetVoltageRampRate(0);
	}
	else
	{
		LeftDrive1->SetVoltageRampRate(14);
		RightDrive1->SetVoltageRampRate(14);
	}
	Stack->SetFeedbackDevice(CANTalon::AnalogPot);
	Stack->SetSensorDirection(false);
	RightShoulder->SetFeedbackDevice(CANTalon::AnalogPot);
	RightShoulder->SetSensorDirection(true);
	LeftShoulder->SetFeedbackDevice(CANTalon::AnalogPot);
	LeftShoulder->SetSensorDirection(true);
	Wrist->SetFeedbackDevice(CANTalon::AnalogPot);
	LeftDrive1->SetFeedbackDevice(CANTalon::QuadEncoder);
	LeftDrive1->SetSensorDirection(true);
	RightDrive1->SetFeedbackDevice(CANTalon::QuadEncoder);

	LeftShoulder->SetVoltageRampRate(60);
	RightShoulder->SetVoltageRampRate(60);

	Wrist->SetVoltageRampRate(1200);


	Stack->SetPosition(Stack->GetAnalogInRaw());
	LeftShoulder->SetPosition(LeftShoulder->GetAnalogInRaw());
	RightShoulder->SetPosition(RightShoulder->GetAnalogInRaw());
	Wrist->SetPosition(Wrist->GetAnalogInRaw());

	LeftDrive1->SetSafetyEnabled(false);
	RightDrive1->SetSafetyEnabled(false);
	LeftFloor->SetSafetyEnabled(false);
	RightFloor->SetSafetyEnabled(false);
	LeftShoulder->SetSafetyEnabled(false);
	RightShoulder->SetSafetyEnabled(false);
	Wrist->SetSafetyEnabled(false);
	Stack->SetSafetyEnabled(false);
	CanStealMotor->SetSafetyEnabled(false);
}

void Robot::EMERGENCY_STOP( void )
{
	// stop all commands
	Scheduler::GetInstance()->RemoveAll();

	// turn off all motors
	LeftDrive1->Set(0);
	RightDrive1->Set(0);
	LeftFloor->Set(0);
	RightFloor->Set(0);
	Stack->Set(0);
	CanStealMotor->Set(0);

	// Make the arm goal wherever it is now
	arm->armGoal = arm->armPosition;
	arm->WristGoal = arm->WristAngle + arm->armPosition;
}


int main()
{
	if (!HALInitialize()){std::cerr<<"FATAL ERROR: HAL could not be initialized"<<std::endl;return -1;}
	HALReport(HALUsageReporting::kResourceType_Language, HALUsageReporting::kLanguage_CPlusPlus);
	robot = new Robot();
	RobotBase::robotSetup(robot);
	return 0;
}
