/*
 * PIDAverage.h
 *
 *  Created on: Feb 14, 2015
 *      Author: roryf_000
 */

#ifndef PIDAVERAGE_H_
#define PIDAVERAGE_H_
#include <PIDSource.h>
#include "WPIlib.h"

class PIDAverage1:public PIDSource
{
public:
	PIDAverage1(CANTalon *L, CANTalon *R, int offsetL, int offsetR, float convertL, float convertR);
	virtual ~PIDAverage1();
	virtual double PIDGet();
	CANTalon *Left;
	CANTalon *Right;
	int offsetLeft;
	int offsetRight;
	float convertLeft;
	float convertRight;
};

#endif /* PIDAVERAGE_H_ */
