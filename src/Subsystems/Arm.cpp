#include "wpilib.h"
#include "subsystems/Arm.h"
#include "DummyPIDOutput.h"
#include "GoalDistance.h"
#include "PIDAverage.h"
#include "PIDDifference.h"

#include "robot.h"
#include "commands/armmanual.h"


Arm::Arm() :
		Subsystem("Arm")
{
	motorSetting = 0;
	FirstManual = true;
	FirstPID = false;
	pidLeft = 0;
	pidRight = 0;
	PIDInitializer = new Timer;
	PIDInitializer->Reset();
	PIDInitializer->Start();
	LeftArm = ((robot->LeftShoulder->GetPosition()+robot->left_arm_pot))*ARM_POT_CONVERSION;
	RightArm = (-(robot->RightShoulder->GetPosition()+robot->right_arm_pot))*ARM_POT_CONVERSION;
	WristAngle = ((robot->Wrist->GetPosition() - robot->wrist_pot)*WRIST_POT_CONVERSION);
	armPosition = (LeftArm + RightArm)/2;
	armGoal = (LeftArm + RightArm)/2;
	WristSetPoint = 95 - ((LeftArm + RightArm)/2);
	PIDAverage = new PIDAverage1(robot->LeftShoulder, robot->RightShoulder, robot->left_arm_pot, robot->right_arm_pot, ARM_POT_CONVERSION, ARM_POT_CONVERSION);
	AveragePIDResult = new DummyPIDOutput();
	DifferencePIDResult = new DummyPIDOutput();
	WristDistanceFromGoal = new DummyPIDOutput();
	WristGoalDistance = new GoalDistance(robot->Wrist);
	PIDDifference = new PIDDifference1(robot->LeftShoulder, robot->RightShoulder, 2);
	ArmAverage = new PIDController(.026, 0.0, 0.0, PIDAverage, AveragePIDResult, .02);//how far from goal
	ArmDifference = new PIDController(.02, 0.0, 0.0, PIDDifference, DifferencePIDResult, .02); //how far from eachother
	ArmDifference->SetSetpoint(0);
	WristShiz = new PIDController(.045, 0.0, 0.0, WristGoalDistance, WristDistanceFromGoal, .02); //how far from goal
	WristShiz->SetAbsoluteTolerance(2.0); //!*!*!* 1.0 for 3-turn
	Initialized = false;
	WristGoal = WristAngle + armPosition;
	ArmAverage->Disable();
	ArmDifference->Disable();
	WristShiz->Disable();
	WristSetting = 0;
	WristFirstPID = true;

}

void Arm::Periodic()
{
	LeftArm = ((robot->LeftShoulder->GetPosition()+robot->left_arm_pot))*ARM_POT_CONVERSION;
	RightArm = ((robot->RightShoulder->GetPosition()+robot->right_arm_pot))*ARM_POT_CONVERSION;
	armPosition = (LeftArm + RightArm)/2;
	WristAngle = ((robot->Wrist->GetPosition() - robot->wrist_pot)*WRIST_POT_CONVERSION);
	WristShiz->SetSetpoint(WristSetPoint);


	if((PIDInitializer->HasPeriodPassed(.2)) && (Initialized == false))
	{
		WristGoal = WristAngle + armPosition;
		WristShiz->Enable();
		ArmAverage->Enable();
		WristShiz->SetSetpoint(WristSetPoint);
		ArmAverage->SetSetpoint(armPosition);
		Initialized = true;
	}

	else if((PIDInitializer->HasPeriodPassed(.2)) && (Initialized == true))
	{
	WristShiz->SetSetpoint(WristSetPoint);
	//armPosition = (LeftArm + RightArm)/2;

	//ArmDifference->SetSetpoint(0)
	//robot->RightShoulder->Set((AveragePIDResult->GetPIDResult()));
	//robot->LeftShoulder->Set(-(AveragePIDResult->GetPIDResult()));

	}

#if ARM_DEBUG
	std::cout << " Arm Aver PID " << AveragePIDResult->GetPIDResult();
	std::cout << " Left is " << LeftArm;
	std::cout << " Right is " << RightArm;
	std::cout << "Left Raw is " << robot->LeftShoulder->GetPosition();
	std::cout << "Right Raw is " <<robot->RightShoulder->GetPosition();
//	std::cout << " Arm Diff PID " << DifferencePIDResult->GetPIDResult() ;
	std::cout << " SetPoint " << ArmAverage->GetSetpoint();
	std::cout << " Source is " <<PIDAverage->PIDGet();
//	std::cout << " good? " << Initialized;
	std::cout << " Wrist PID " << WristDistanceFromGoal->GetPIDResult();
	std::cout << " Wrist Goal " << WristGoal;
	std::cout << " Wrist Set " << WristShiz->GetSetpoint();
	std::cout << " wristCurrent=" << robot->Wrist->GetOutputCurrent();
	std::cout << " Wrist " << WristAngle << "\n";
	std::cout << "\n";
#endif

/*	SmartDashboard::PutData("Arm Average PID", ArmAverage);
	SmartDashboard::PutData("Arm Difference PID", ArmDifference);
	SmartDashboard::PutNumber("Left Arm Position", LeftArm);
	SmartDashboard::PutNumber("Right Arm Position", RightArm);
	SmartDashboard::PutData("Wrist PID", WristShiz);
	SmartDashboard::PutNumber("Wrist Position", WristAngle);*/

	float pid = AveragePIDResult->GetPIDResult();
	pidLeft = pid;
	pidRight = -pid;

	float WristPid = WristDistanceFromGoal->GetPIDResult();




	if(robot->JsOperator->GetRawAxis(1) >= .05 || robot->JsOperator->GetRawAxis(1) <= -.05)
	{
		float armSpeed;
		armSpeed = robot->JsOperator->GetRawAxis(1);
		armSpeed = ((.6*armSpeed*armSpeed*armSpeed) +(.4*armSpeed))/1.75;
		FirstPID = true;
		if(FirstManual == true)
		{
			robot->LeftShoulder->SetVoltageRampRate(12);
			robot->RightShoulder->SetVoltageRampRate(12);
			FirstManual = false;
		}
		ManualRelease.Reset();
		ManualRelease.Start();
		robot->arm->ArmAverage->SetSetpoint(robot->arm->armPosition);
		robot->LeftShoulder->Set(armSpeed);
		robot->RightShoulder->Set(-armSpeed);
		motorSetting = ((abs(armSpeed)) +.2);

		//std::cout <<"Swag" << "/n";
	}
	else
	{
		if(FirstPID == true)
		{
			robot->RightShoulder->Set(0);
			robot->LeftShoulder->Set(0);
			if(ManualRelease.HasPeriodPassed(motorSetting))
			{
				robot->LeftShoulder->SetVoltageRampRate(40);
				robot->RightShoulder->SetVoltageRampRate(40);
				robot->arm->ArmAverage->SetSetpoint(robot->arm->armPosition);
				FirstManual = true;
				FirstPID = false;
			}
		}
		else if(ArmAverage->IsEnabled() == true)
		{
			if( robot->arm->pidLeft > 0.65 )
			{
				robot->arm->pidLeft = 0.65;
			}
			else if( robot->arm->pidLeft < -0.65 )
			{
				robot->arm->pidLeft = -0.65;
			}
			if( robot->arm->pidRight > 0.65 )
			{
				robot->arm->pidRight = 0.65;
			}
			else if( robot->arm->pidRight < -0.65 )
			{
				robot->arm->pidRight = -0.65;
			}
			robot->LeftShoulder->Set(robot->arm->pidLeft);
			robot->RightShoulder->Set(robot->arm->pidRight);
		}
		else
		{
			robot->LeftShoulder->Set(0);
			robot->RightShoulder->Set(0);
		}
	}

	//Where the Magic happens
	if((robot->JsOperator->GetRawAxis(2) > .1) ) //!*!*!* && (armPosition > 58 || armPosition < -58))
	{
		WristFirstPID = true;
		robot->Wrist->Set(robot->JsOperator->GetRawAxis(2));
		WristGoal = WristAngle + armPosition;
		WristSetting = robot->JsOperator->GetRawAxis(2);
	}
	else if( (robot->JsOperator->GetRawAxis(3) > .1) ) //!*!*!* && (armPosition > 58 || armPosition < -58))// && WristGoal > 88)) //If this doesnt work flip the signs on the wrist goal comaprrison
	{
		WristFirstPID = true;
		robot->Wrist->Set(-(robot->JsOperator->GetRawAxis(3)));
		WristGoal = WristAngle + armPosition;
		WristSetting = robot->JsOperator->GetRawAxis(3);
	}

	else
	{
		if(WristFirstPID == true)
		{
			WristGoal = WristAngle + armPosition;
			robot->Wrist->Set(0);
			WristSetPoint = (WristGoal - armPosition);
			WristFirstPID = false;
		}
		else if(WristShiz->IsEnabled())
		{

			WristSetPoint = (WristGoal - armPosition);
			float FeedManualArm;
			FeedManualArm = 1.25 * robot->LeftShoulder->Get();
			if( (FeedManualArm < 0.1) && (FeedManualArm > -0.1) )
			{
				FeedManualArm = 0.0;
			}
			else if(FeedManualArm > 1)
			{
				FeedManualArm = 1.0;
			}
			else if( FeedManualArm < -1.0 )
			{
				FeedManualArm = -1.0;
			}

			robot->Wrist->Set(WristPid - FeedManualArm);
//			robot->Wrist->Set(0);
		}


#if 0
	else if((armPosition < 58 && armPosition > -58))// && (WristGoal < 88)) //If this doesnt work flip the signs on wrist goal comparisson
	{
		WristGoal = 80;
		WristSetPoint = (WristGoal - armPosition);
		float FeedManualArm;
		FeedManualArm = (1*robot->LeftShoulder->Get());
		if(FeedManualArm < 0.1)
		{
			FeedManualArm = 0.0;
		}
		else if(FeedManualArm > 1)
		{
			FeedManualArm = 1.0;
		}
		robot->Wrist->Set(Wris	Pid - FeedManualArm);
	}
#endif


	}



}

void Arm::InitDefaultCommand()
{
	// Set the default command for a subsystem here.
	//SetDefaultCommand(new ArmManual() );
}

// Put methods for controlling this subsystem
// here. Call these from Commands.

void Arm::ArmController( double left, double right )
{

}

//	LeftArm = (abs(robot->LeftShoulder.GetAnalogIn()-530))/1.5;
//	RightArm = (abs(robot->RightShoulder.GetAnalogIn()-530))/1.5;
