#include "DriveTrain.h"
#include "Commands/DriveManual.h"
#include "robot.h"

DriveTrain::DriveTrain() :
		Subsystem("DriveTrain")
{
	drive = new RobotDrive( robot->LeftDrive1, robot->RightDrive1 );
	drive->SetSafetyEnabled(false);
	drive->SetExpiration(0.1);
	drive->SetSensitivity(1.0);
	drive->SetMaxOutput(1.0);
}

void DriveTrain::InitDefaultCommand()
{
	SetDefaultCommand( new DriveManual() );
}

// This is called for manual drive control
void DriveTrain::TankDrive(Joystick* joy)
{
	double left, right;

	left = joy->GetRawAxis(1); //* robot->driverJoystickDivider;
	right = -joy->GetRawAxis(5); // * robot->driverJoystickDivider * -1.0;

	drive->TankDrive( left, right );
}

// This is called for autonomous or teleop sequences
void DriveTrain::TankDrive( double leftAxis, double rightAxis )
{
	double clipLeft, clipRight;

	clipLeft = leftAxis;
	clipRight = rightAxis;

	if( clipLeft > 0)
	{
		if( clipLeft < 0.1 ) clipLeft = 0;// else clipLeft = clipLeft - 0.1;
	}
	if( clipLeft < 0 )
	{
		if( clipLeft > -0.1 ) clipLeft = 0;// else clipLeft = clipLeft + 0.1;
	}

	if( clipRight > 0)
	{
		if( clipRight < 0.1 ) clipRight = 0;// else clipRight = clipRight - 0.1;
	}
	if( clipRight < 0 )
	{
		if( clipRight > -0.1 ) clipRight = 0;// else clipRight = clipRight + 0.1;
	}

	drive->TankDrive( clipLeft, clipRight );
	//drive->TankDrive(leftAxis, rightAxis);
}


void DriveTrain::Stop()
{
	drive->TankDrive(0.0, 0.0);
}

int DriveTrain::GetLeftEncoder()
{
	return robot->LeftDrive1->GetPosition();
}

int DriveTrain::GetRightEncoder()
{
	return robot->RightDrive1->GetPosition();
}

double DriveTrain::GetAngle()
{
	return robot->gyro->GetAngle();
}
