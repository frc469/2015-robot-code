#ifndef Dummy_H
#define Dummy_H

#include "Commands/Subsystem.h"
#include "WPILib.h"

class Dummy: public Subsystem
{
private:
	// It's desirable that everything possible under private except
	// for methods that implement subsystem capabilities
public:
	Dummy();
	void InitDefaultCommand();
};

#endif
