#ifndef CanSteal_H
#define CanSteal_H

#include "Commands/Subsystem.h"
#include "WPILib.h"

class CanSteal: public Subsystem
{
private:
	// It's desirable that everything possible under private except
	// for methods that implement subsystem capabilities
public:
	CanSteal();
	void InitDefaultCommand();
};

#endif
