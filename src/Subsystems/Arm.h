#ifndef Arm_H
#define Arm_H

#include "Commands/Subsystem.h"
#include "WPILib.h"
#include "DummyPIDOutput.h"
#include "GoalDistance.h"
#include "PIDAverage.h"
#include "PIDDifference.h"

class Arm: public Subsystem
{
private:
	bool FirstManual;
	bool FirstPID;
	double WristSetPoint;
	double LeftArm;

	double WristFirstPID;



	Timer ManualRelease;
	float motorSetting;
	Timer WristRelease;
	float WristSetting;

	PIDAverage1 *PIDAverage;
	PIDDifference1 *PIDDifference;

	GoalDistance *WristGoalDistance;
	// It's desirable that everything possible under private except
	// for methods that implement subsystem capabilities
public:
	bool Initialized;
	double RightArm;
	DummyPIDOutput *AveragePIDResult;
	DummyPIDOutput *DifferencePIDResult;
	DummyPIDOutput *WristDistanceFromGoal;
	float pidLeft;
	double WristGoal;
	bool GripOpen;
	float pidRight;
	Timer *PIDInitializer;
	double armPosition;
	double WristAngle;
	PIDController *ArmAverage;
	PIDController *ArmDifference;
	PIDController *WristShiz;
	double armGoal;
	Arm();
	void ChangeSetPoint();
	void Periodic();
	void InitDefaultCommand();
	void ArmController( double left, double right );
};

#endif
