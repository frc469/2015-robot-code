#include "robot.h"
#include <Subsystems/Stacker.h>
#include "commands/StackerManual.h"
#include "commands/StackerPosition.h"

Stacker::Stacker() : Subsystem("Stacker")
{
}

void Stacker::InitDefaultCommand()
{
	// Set the default command for a subsystem here.
	SetDefaultCommand(new StackerManual());
}

// Put methods for controlling this subsystem
// here. Call these from Commands.
void Stacker::StackerOutput( double stackerMotor )
{
	robot->Stack->Set( stackerMotor );
}

#if 0
elevatorPosition = Stack.GetAnalogInRaw();
if( elevatorAutomatic == true )
{
	// automatic elevator control
	if( (abs(elevatorPosition - elevatorSetpoint) < ELEVATOR_TOLERANCE) ||
		(elevatorTimer.Get() > 10.0) )
	{
		elevatorSpeed = 0;
		elevatorAutomatic = false;
		elevatorTimer.Stop();
		elevatorTimer.Start();
	}
	else if( elevatorPosition > elevatorSetpoint )
	{
		elevatorSpeed = -1;
		StackUp = false;
	}
	else
	{
		elevatorSpeed = 1;
		StackUp = true;
	}
	if( (abs( elevatorPosition - elevatorSetpoint) < 110 ) && StackUp == false)
	{
		elevatorSpeed = elevatorSpeed / 3;
	}
	Stack.Set( elevatorSpeed );
}
else // elevatorAutomatic == false
{
	// manual elevator control
	if((JsDriver.GetRawAxis(2) >= .1) && (elevatorPosition < ELEVATOR_HARD_TOP))
	{
		Stack.Set(1 * JsDriver.GetRawAxis(2));
	}
	else if((JsDriver.GetRawAxis(3) >=.1) && (elevatorPosition > ELEVATOR_HARD_BOTTOM))
	{
		Stack.Set(-1 * JsDriver.GetRawAxis(3));
	}
	else if (elevatorPosition <= ELEVATOR_HARD_BOTTOM || elevatorPosition >= ELEVATOR_HARD_TOP)
	{
		Stack.Set(0);
	}
	else
	{
		Stack.Set(0);
	}
}
if((JsDriver.GetRawAxis(3) >=.1) || (JsDriver.GetRawAxis(2) >=.1))
{
	elevatorAutomatic = false;
	FloorStep = 0;
	elevatorSetpoint = elevatorPosition;
}

#endif
