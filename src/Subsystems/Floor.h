#ifndef Floor_H
#define Floor_H

#include "Commands/Subsystem.h"
#include "WPILib.h"

class Floor: public Subsystem
{
private:
	// It's desirable that everything possible under private except
	// for methods that implement subsystem capabilities
public:
	Floor();
	void InitDefaultCommand();
};

#endif
