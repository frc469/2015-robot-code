#ifndef Stacker_H
#define Stacker_H

#include "Commands/Subsystem.h"
#include "WPILib.h"

class Stacker: public Subsystem
{
private:
	// It's desirable that everything possible under private except
	// for methods that implement subsystem capabilities
	int stackerPosition;
	int stackerSetpoint;

public:
	Stacker();
	void InitDefaultCommand();
	void StackerOutput( double stackerMotor );

	static const int STACKER_TOLERANCE = 20;
	static const int STACKER_TIMEOUT = 10.0;
};


#endif
