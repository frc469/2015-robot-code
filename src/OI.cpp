#include <Commands/StackOpenClose.h>
#include "OI.h"

#include "robot.h"

#include "subsystems/stacker.h"
#include "subsystems/arm.h"

#include "Commands/StackerPosition.h"
#include "Commands/ShiftGear.h"
#include "Commands/FloorMotor.h"
#include "Commands/StackOpenClose.h"
#include "Commands/FloorOpenClose.h"
#include "Commands/FloorLoadAutomated.h"
#include "Commands/GripToggle.h"
#include "Commands/ArmSet.h"
#include "Commands/Interrupt.h"
#include "Commands/bumpcheck.h"
#include "Commands/Btn1.h"
#include "Commands/Btn2.h"
#include "Commands/Btn3.h"
#include "Commands/Btn7.h"
#include "Commands/Btn8.h"
#include "commands/KillWristPID.h"
#include "Commands/KillArmPID.h"
#include "Commands/FireOJ.h"
#include "Commands/WinchCanSteal.h"

OI::OI()
{

	//Driver Controls

	gearButton = new JoystickButton( robot->JsDriver, 1 );
	gearButton->WhenPressed( new FloorOpenClose(0) );
//	gearButton->WhenPressed( new BumpCheck(1) );

	floorLoadAutoButton = new JoystickButton( robot->JsDriver, 2 );
	floorLoadAutoButton->WhenPressed( new FloorLoadAutomated(1) );

	floorLoadAutoButton = new JoystickButton( robot->JsDriver, 3 );
	floorLoadAutoButton->WhenPressed( new FloorLoadAutomated(5) );
//	floorLoadAutoButton->WhenPressed( new BumpCheck(5) );

//	load1Button = new JoystickButton( robot->JsDriver, 3 );
//	load1Button->WhenPressed( new StackerLoad1 );

//	clear1Button = new JoystickButton( robot->JsDriver, 4 );
//	clear1Button->WhenPressed( new StackerClear1 );

	floorOpenCloseToggle = new JoystickButton( robot->JsDriver, 4 );
	floorOpenCloseToggle->WhenPressed( new FloorLoadAutomated(4) );
//	floorOpenCloseToggle->WhenPressed( new BumpCheck(4) );

	floorOutButton = new JoystickButton( robot->JsDriver, 5 );
	floorOutButton->WhenPressed( new FloorMotor( FloorMotor::floorOut, true ) );

	floorInButton = new JoystickButton( robot->JsDriver, 6 );
	floorInButton->WhenPressed( new FloorMotor( FloorMotor::floorIn, true ) );

	DriverInterrupt = new JoystickButton( robot->JsDriver, 7);
	DriverInterrupt->WhenPressed(new Interrupt());

	OperatorButton1 = new JoystickButton( robot->JsOperator, 1 );
	OperatorButton1->WhenPressed( new Btn1() );
	robot->Btn1Mode1 = new ArmSet(-121, -80);
//	robot->Btn1Mode1 = new FloorOpenClose(0);
	robot->Btn1Mode2 = new ArmSet(-121, -80);

	OperatorButton2 = new JoystickButton( robot->JsOperator, 2 );
	OperatorButton2->WhenPressed( new Btn2() );
	robot->Btn2Mode1 = new StackOpenClose(0);
	robot->Btn2Mode2 = new StackOpenClose(0);
//	robot->Btn2Mode2 = new ArmSet(0, -90);

	OperatorButton3 = new JoystickButton( robot->JsOperator, 3);
	OperatorButton3->WhenPressed( new Btn3() );
//	robot->Btn3Mode1 = NULL;
	robot->Btn3Mode2 = new ArmSet(-100, -90);
	robot->Btn3Mode1 = new ArmSet(-100, -90);

	OperatorButton4 = new JoystickButton( robot->JsOperator, 4 );
	OperatorButton4->WhenPressed( new GripToggle(0) );

	OperatorButton7 = new JoystickButton( robot->JsOperator, 7 );
	OperatorButton7->WhenPressed(new Btn7());
	robot->Btn7Mode1 = new ArmSet( -87.5, -180) ;
//	robot->Btn7Mode1 = new KillWristPID();
	robot->Btn7Mode2 = new ArmSet( -87.5, -180) ;

//	OperatorButton8 = new JoystickButton( robot->JsOperator, 8 );
//	OperatorButton8->WhenPressed( new Btn8()  );
//	robot->Btn8Mode2 = new TestFire();
//	robot->Btn8Mode1 = new TestFire();
//	robot->Btn8Mode1 = new KillArmPID();

	//ArmSet90 = new JoystickButton(robot->JsOperator, 3);
	//ArmSet90->WhenPressed( new ArmSet(90));

	//ArmSet0 = new JoystickButton(robot->JsOperator, 4);
	//ArmSet0->WhenPressed( new ArmSet(0) );

	//landfillGrab = new JoystickButton( robot->JsOperator, 3);
	//landfillGrab->WhileHeld( new FloorMotor(FloorMotor::floorRight, false) );
	//landfillGrab->WhenReleased(new FloorMotor(FloorMotor::floorStop, false) );

	//landfillGrab2 = new JoystickButton( robot->JsOperator, 8);
	//landfillGrab2->WhileHeld( new FloorMotor(FloorMotor::floorLeft, false));
	//landfillGrab2->WhenReleased( new FloorMotor(FloorMotor::floorStop, false));



	floorOutButton = new JoystickButton( robot->JsOperator, 6 );
	floorOutButton->WhenPressed( new FloorMotor( FloorMotor::floorRight, true ) );

	floorInButton = new JoystickButton( robot->JsOperator, 5 );
	floorInButton->WhenPressed( new ArmSet(0, -90) );
//	floorInButton->WhenPressed( new FloorMotor( FloorMotor::floorIn, true ) );

	// E-stop: clear out all a*ctions
	//Scheduler::GetInstance()->Run();

	// SmartDashboard Buttons
	//SmartDashboard::PutData("Drive Forward", new DriveForward(2.25));
}
