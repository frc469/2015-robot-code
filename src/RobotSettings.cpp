/*
f * RobotSettings.c
 *
 *  Created on: Mar 16, 2015
 *      Author: Neal
 */



#include "robot.h"

#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

using namespace std;

void RobotSettings( void )
{

	const char delimiters[] = "\t "; // search pattern
	string line;
	char buf[80];
	ifstream Myfile;
	Myfile.open ("/home/lvuser/robotsettings.txt");

	if(Myfile.is_open())
	{
		while(!Myfile.eof())
		{
			getline(Myfile,line);

			// parse the line into blank-delimited tokens

			// array to store memory addresses of the tokens in buf
			const char *parameter;
			const char *value;
			int num;

			strncpy( buf, line.c_str(), 79);
			buf[79] = 0;

			// parse the line
			parameter = strtok(buf, delimiters); // first token
			if (parameter != NULL) // if not blank
			{
				value = strtok(NULL, delimiters); // subsequent token
				if (value != NULL)
				{
					std::cout << "parameter=" << parameter << " value=" << value << "\n";
														if( !strcmp( parameter, "LEFT_ARM_POT") )
					{
						num = atol( value );
						if( num )
						{
							robot->left_arm_pot = num;
							std::cout << "LEFT_ARM_POT set to " << robot->left_arm_pot << "\n";
						}
					}
					else if( !strcmp( parameter, "RIGHT_ARM_POT") )
					{
						num = atol( value );
						if( num )
						{
							robot->right_arm_pot = num;
							std::cout << "RIGHT_ARM_POT set to " << robot->right_arm_pot << "\n";
						}
					}
					else if( !strcmp( parameter, "WRIST_POT") )
					{
						num = atol( value );
						if( num )
						{
							robot->wrist_pot = num;
							std::cout << "WRIST_POT set to " << robot->wrist_pot << "\n";
						}
					}
					else if( !strcmp( parameter, "STACKER_HARD_TOP") )
					{
						num = atol( value );
						if( num )
						{
							robot->stacker_hard_top = num;
							std::cout << "STACKER_HARD_TOP set to " << robot->stacker_hard_top << "\n";
						}
					}
					else if( !strcmp( parameter, "AUTON") )
					{
						num = atol( value );
						if( num )
						{
							robot->Auton = num;
							std::cout << "Auton set to " << robot->Auton << "\n";
						}
					}
				}
			}
		}
		Myfile.close();
	}
	else
	{
		std::cout<<"Unable to open this file.\n";
	}

}
