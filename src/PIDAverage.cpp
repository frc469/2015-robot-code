/*
 * PIDAverage.cpp
 *
 *  Created on: Feb 14, 2015
 *      Author: roryf_000
 */

#include "PIDAverage.h"
#include "Robot.h"
#include <iostream>

PIDAverage1::PIDAverage1(CANTalon *L, CANTalon *R, int offsetL, int offsetR, float convertL, float convertR) {
	// TODO Auto-generated constructor stub
	Left = L;
	Right = R;
	offsetLeft = offsetL;
	offsetRight = offsetR;
	convertLeft = convertL;
	convertRight = convertR;


}


PIDAverage1::~PIDAverage1() {
	// TODO Auto-generated destructor stub
}
double PIDAverage1::PIDGet()
{

		double LeftPos = convertLeft*(Left->GetPosition() + offsetLeft);
		double RightPos = convertRight*(Right->GetPosition() + offsetRight);
		//std::cout << "Encoders are " << Left->GetEncPosition() << " " << Right->GetEncPosition() << "\n";
		double Goal = ((LeftPos+RightPos)/2);

		return Goal;


	//return Goal;
}


