/*
 * GyroSource.h
 *
 *  Created on: Apr 4, 2015
 *      Author: roryf_000
 */

#ifndef GYROSOURCE_H_
#define GYROSOURCE_H_
#include "wpilib.h"

class GyroSource:public PIDSource

{
private:
	Gyro *gyro;
public:
	GyroSource(Gyro *G);
	virtual double PIDGet();
	virtual ~GyroSource();
};



#endif /* GYROSOURCE_H_ */
