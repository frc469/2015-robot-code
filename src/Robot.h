/*
 * Robot.h
 *
 *  Created on: Feb 25, 2015
 *      Author: Neal
 */

#ifndef COMMANDROBOT2_26_SRC_ROBOT_H_
#define COMMANDROBOT2_26_SRC_ROBOT_H_

#define DRIVE_FORWARD_DEBUG 1
#define ARM_DEBUG 0
#define ROBOT_TYPE 0 //0 == practice 1 == comp

// =======================================================
// Put things that need to be tweaked per robot here
// - pot offsets, etc

//#define STACKER_HARD_BOTTOM (170)
//#define ARM_LEFT_OFFSET (534)
//#define ARM_RIGHT_OFFSET (626)
//#define WRIST_OFFSET (558)
// =======================================================

#define ENCODER_CONVERSION (1.0/436.25) //400 turns = 1 inch
#define ARM_POT_CONVERSION (180.0/277.0) // 276 counts = 180 degrees
#define WRIST_POT_CONVERSION (180.0/425.0) // 434 for 3-turn (was 134) counts = 180 416 for practice

#include <Subsystems/Stacker.h>
#include "WPILib.h"
#include <iostream>

#include "WPILib.h"
#include "Commands/Command.h"

#include "Subsystems/DriveTrain.h"
#include "subsystems/Arm.h"
#include "subsystems/CanSteal.h"
#include "subsystems/Floor.h"
#include "subsystems/Stacker.h"
#include "subsystems/Dummy.h"
#include "oi.h"

class CANTalonSafe: public CANTalon
{
private:
	float lastValue;
	int ControllerMode;

public:
	CANTalonSafe( int deviceNumber ) : CANTalon( deviceNumber )
	{
		lastValue = 0;
	}
	virtual float Get() override
	{
		float safeValue;
		safeValue = lastValue;
		try
		{
			if( CANTalon::IsAlive() )
			{
				safeValue = CANTalon::Get();
			}
		}
		catch( int e )
		{
		}
		return safeValue;
	}
};

class Robot: public IterativeRobot {

public:
	double FloorSpeed;
	double DriveSpeedTest;
	double DriveTime;
	int Test1;
	//Members in da club
	bool FloorFinished;
	bool FloorOpen;
	int LogCount;
	bool ArmIsWorking;
	double ControllerDPAD;
	double p;
	double i;
	double d;
	int izone;
	double ramprate;
	int profile;
	bool gearHigh;
    bool floorClosed;
    int OperatorMode;
    int left_arm_pot;
    int right_arm_pot;
    int wrist_pot;
    int stacker_hard_top;
	int stacker_load_1;
	int stacker_clear_1;
	int stacker_load_2;
	int stacker_clear_2;
	int stacker_hard_bottom;
	int stacker_floor_clear;
	int stacker_bedford_falls;
	int stacker_bedford_falls2;
	int DriverPrevPOV;
	int OpPrevPOV;
	double driverJoystickDivider;
	Timer AutonTimer;
	bool autonCurrentScan;
	double DriveSpeed;
	bool emergencyStop;
	bool autoFinished;
	bool StateOfTheOJ;
	ControllerPower *CheckPower;
	DigitalInput *LeftLimitSwitch;
	DigitalInput *RightLimitSwitch;
	bool Initialize;
	Timer *DisableTime;
	int Auton;
	double AutonLoops;
	//JoySticks
	Joystick *JsDriver;
	Joystick *JsOperator;

	//Talons
	CANTalonSafe *LeftDrive1;
	CANTalonSafe *RightDrive1;
	//CANTalonSafe *LeftDrive2;
	//CANTalonSafe *RightDrive2;
	CANTalonSafe *LeftFloor;
	CANTalonSafe *RightFloor;
	CANTalonSafe *LeftShoulder;
	CANTalonSafe *RightShoulder;
	CANTalonSafe *Wrist;
	CANTalonSafe *Stack;
	CANTalonSafe *CanStealMotor;

	//Pneumatics
	Solenoid *Drive;
	DoubleSolenoid *OJRight;
	Solenoid *FloorSolenoid;
	DoubleSolenoid *OJ;
	DoubleSolenoid *StackGrab;
	DoubleSolenoid *CanGrab;

	Gyro *gyro;

	static DriveTrain *drivetrain;
	static Arm *arm;
	static CanSteal *cansteal;
	static Floor *floorload;
	static Stacker *stacker;
	static Dummy *dummy;
	static OI *oi;
	LiveWindow *lw;

	//Mode Commands
	Command *Btn1Mode1;
	Command *Btn1Mode2;
	Command *Btn2Mode1;
	Command *Btn2Mode2;
	Command *Btn3Mode1;
	Command *Btn3Mode2;
	Command *Btn7Mode1;
	Command *Btn7Mode2;
	Command *Btn8Mode1;
	Command *Btn8Mode2;
	Command *DrivePOV;
	Command *BtnDriveSpeedToggle;
	Command *ToggleOJ;
	Command *WinchSteal;
	Command *WristSet90;
	Command *WristSet180;


private:
	Command *autonomousCommand;
	SendableChooser* autoChooser;


	void RobotInit();
	void AutonomousInit();
	void AutonomousPeriodic();
	void TeleopInit();
	void TeleopPeriodic();
	void TestPeriodic();
	void DisabledInit();
	void DisabledPeriodic();

	void TalonInit(int M);
	void Log();

	void EMERGENCY_STOP( void );
};

extern Robot *robot;


#endif /* COMMANDROBOT2_26_SRC_ROBOT_H_ */
