/*
 * PIDDifference.h
 *
 *  Created on: Feb 15, 2015
 *      Author: roryf_000
 */

#ifndef PIDDIFFERENCE_H_
#define PIDDIFFERENCE_H_
#include <PIDSource.h>
#include "WPIlib.h"

class PIDDifference1 : public PIDSource
{
public:
	PIDDifference1(CANTalon *L, CANTalon *R, int C);
	virtual double PIDGet();
	virtual ~PIDDifference1();
	CANTalon *Left;
	CANTalon *Right;
	double Conversion;
};

#endif /* PIDDIFFERENCE_H_ */
