#ifndef OI_H
#define OI_H

#include "WPILib.h"

class OI {
private:

	JoystickButton *gearButton;
	JoystickButton *load1Button;
	JoystickButton *clear1Button;
	JoystickButton *floorInButton;
	JoystickButton *floorOutButton;
	JoystickButton *stackOpenButton;
	JoystickButton *floorOpenCloseToggle;
	JoystickButton *floorOpenButton;
	JoystickButton *floorLoadAutoButton;
	JoystickButton *ArmSet90;
	JoystickButton *ArmSet0;
	//JoystickButton *landfillGrab;
	//JoystickButton *landfillGrab2;
	JoystickButton *DriverInterrupt;

	JoystickButton *OperatorButton1;
	JoystickButton *OperatorButton2;
	JoystickButton *OperatorButton3;
	JoystickButton *OperatorButton4;
	JoystickButton *OperatorButton7;
	JoystickButton *OperatorButton8;

public:
	OI();
	Joystick* GetJoystick();
};

#endif
